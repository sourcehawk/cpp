#ifndef HW4_PERSON_H
#define HW4_PERSON_H
#include "Being.h"
#include "../json.hpp"
#include "../role_builder_models/beingRole.h"
#include "../role_builder_models/personRole.h"
#include "../character_builder_models/beingCharacter.h"
#include "../character_builder_models/personCharacter.h"
using namespace std;
/*
 * life
 * strength
 * intelligence
 * type
 * name
 * gender
 * fear
 * ----
 * role
 */
class Person: public Being {
public:
    // Constructor for role loading
    Person(role_being::loader& being_rl, role_person::loader& role_loader);

    // Constructor for character loading
    Person(char_being::loader& being_cl, char_person::loader& char_loader);

    // Returns true if person has a name
    bool named() const;
    // Returns true if person is an investigator
    bool investigator() const;
    // Reshuffles the persons fear stat
    void reRandomize();

    void changeFear(int newFear);
    void changeGender(string newGender);
    void changeName(string newName) override;
    void changeTerror(int newTerror);
    void makeInvestigator();



    void to_json(json& j) override;


    char_person::loader* getPersonData();

protected:
    int fear=0;
    int terror=0;
    string gender;

private:
    int maxTerror = 3;
    int maxFear = 10;
    bool hasGender = false;
    bool hasName = false;
    bool isInvestigator = false;
};

#endif //HW4_PERSON_H
