#include "Person.h"
#include <iostream>

Person::Person(role_being::loader &being_rl, role_person::loader &role_loader): Being(being_rl) {
    role = role_loader.Role;
    addAttribute("Role", role);
}

Person::Person(char_being::loader &being_cl, char_person::loader &char_loader):Being(being_cl) {
    changeGender(char_loader.Gender);
    name = char_loader.Name;
    fear = char_loader.Fear;
    role = char_loader.Role;
    addAttribute("Role", role);
    addAttribute("Name", name);
    addAttribute("Gender", gender);
    addAttribute("Fear", fear);
    hasName=true;
    hasGender=true;
    if (type == "Investigator") {
        terror = char_loader.Terror;
        addAttribute("Terror", terror);
    }
    else type = "Person";
}

bool Person::named() const {
    return hasName;
}

bool Person::investigator() const {
    return isInvestigator;
}

void Person::reRandomize() {
    if (isInvestigator){
        terror = random_number(0, 3);
    }
    fear = random_number(0, 10);
}

void Person::changeFear(int newFear) {
    if (newFear<=this->maxFear && newFear>=0){
        fear = newFear;
        return;
    }
    invalid_message("fear");
}

void Person::changeGender(string newGender) {
    gender = move(newGender);
    hasGender=true;
}

void Person::changeName(string newName) {
    Being::changeName(newName);
    hasName = true;
}

void Person::changeTerror(int newTerror) {
    if (!isInvestigator){
        cout << "This person does not have any terror stat! Perhaps you should make an investigator first." << endl;
    }
    else if (newTerror<=maxTerror && newTerror>=0){
        terror = newTerror;
    }
    else{
        invalid_message("terror");
    }
}

void Person::makeInvestigator() {
    if (!hasName || !hasGender){
        cout << "Must assign name and gender before making investigator!" << endl;
        return;
    }
    type = "Investigator";
    terror = random_number(0, 3);
    addAttribute("Terror", terror);
    isInvestigator = true;
}

void Person::to_json(json &j) {
    if ((!hasName || !hasGender) && !isRole){
        cout << "Must assign name and gender!" << endl;
        return;
    }
    // All persons store this attribute
    if (!isInvestigator){
        terror=0;
        addAttribute("Terror", terror);
    }
    Being::to_json(j);
}


char_person::loader *Person::getPersonData() {
    auto* person_data = new char_person::loader;
    person_data->Terror = terror;
    person_data->Role = role;
    person_data->Fear= fear;
    person_data->Name = name;
    person_data->Gender = gender;
    return person_data;
}

