# By
- Hákon Hákonarson
- Ægir Máni Hauksson

# COMPILE COMMAND
`g++ -o test main.cpp loaders/Characters.cpp loaders/Roles.cpp interface/Interface.cpp categories/Being.cpp categories/Person.cpp categories/Creature.cpp categories/EldritchHorror.cpp -std=c++11`

# State diagram of interface
![image](state_diagram.png)