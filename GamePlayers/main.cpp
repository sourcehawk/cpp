#include <cstdlib>
#include <chrono>
#include "interface/Interface.h"

int main() {
    std::srand(chrono::system_clock::to_time_t(chrono::system_clock::now()));
    Interface interface;
    interface.run();
    return 0;
}

