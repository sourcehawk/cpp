#ifndef HW4_CREATURECHARACTER_H
#define HW4_CREATURECHARACTER_H
#include "../json.hpp"
using json = nlohmann::json;


namespace char_creature {
    struct loader {
        string Name;
        string Role;
        bool Unnatural;
        int Disquiet;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(loader, Name, Role, Unnatural, Disquiet)
}
#endif //HW4_CREATURECHARACTER_H
