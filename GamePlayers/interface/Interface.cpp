#include "Interface.h"
#include <iostream>
#include <fstream>
#include <stdexcept>

// TODO: Check if all enums have invalid, default and main menu = -1
// TODO: check if all menus have a title
Interface::Interface() {
    auto* new_roles = new Roles("characters");
    roles = new_roles;
    auto* new_characters = new Characters(*new_roles);
    characters = new_characters;
}

void Interface::run() {
    while(!wantsQuit) { mainMenu();}
}

void Interface::mainMenu() {
    characterSelected = false;
    selected_type = NONE;
    std::cout << std::endl << " -- MAIN MENU --" << std::endl;
    std::cout << "-1: Quit" << std::endl;
    std::cout << " 1: Find individual" << std::endl;
    std::cout << " 2: Save current roster" << std::endl;
    std::cout << " 3: Load character roster" << std::endl;
    std::cout << " 4: Print Menu" << std::endl;
    std::cout << " 5: Create role" << std::endl;
    std::cout << " 6: Delete role" << std::endl;
    std::cout << " 7: Create character" << std::endl;

    wantsMainMenu = false;
    enum action { INVALID, QUIT=-1, FIND=1, SAVE, LOAD, PRINT, CREATE_ROLE, DELETE_ROLE, CREATE_CHARACTER };
    auto user_action = action(getNumberInput());
    switch (user_action){
        case INVALID: {
            invalidNumber();
            break;
        }

        case QUIT: {
            wantsQuit = true;
            return;
        }

        case FIND: {
            while(!wantsMainMenu) {find();}
            break;
        }

        case SAVE:{
            save();
            break;
        }

        case LOAD: {
            while(!wantsMainMenu) load();
            break;
        }

        case PRINT: {
            while(!wantsMainMenu) {print();}
            break;
        }

        case CREATE_ROLE: {
            while(!wantsMainMenu){ createRole(); }
            break;
        }

        case DELETE_ROLE:{
            while(!wantsMainMenu) { deleteRole(); }
            break;
        }

        case CREATE_CHARACTER:{
            while(!wantsMainMenu) { createCharacter(); }
            break;
        }

        default: {
            invalidNumber();
            break;
        }
    }

}

void Interface::find() {
    std::cout << std::endl << "-- FIND CHARACTER --" << std::endl;
    mainMenuCommand();
    roles->printTypes();
    enum types { INVALID, MAIN_MENU=-1, INVESTIGATOR=1, PERSON, CREATURE, ELDRITCH };
    auto action = types(getNumberInput());
    switch(action){
        case INVALID: {
            invalidNumber(); break;
        }

        case MAIN_MENU: {
            wantsMainMenu = true;
            return;
        }

        case PERSON: {
            std::cout << std::endl << "-- PERSON SELECTION --" << std::endl;
            try{
                characters->printAllCharOfType("Person");
            }
            catch (const std::runtime_error& error){
                std::cout << error.what() << std::endl;
                return;
            }
            auto selected_index = getNumberInput();
            if (selected_index == -1){
                wantsMainMenu = true;
                return;
            }
            else if (selected_index <= 0 || selected_index-1>characters->getMaxIndex("Person")){
                invalidNumber();
                return;
            }
            else {
                selected_person = characters->getPersonCharacter(selected_index-1);
                selected_being = selected_person;
                selected_type = PERS;
                characterSelected = true;
                if (!characters->containsAny("Person")){
                    std::cout << "No persons!" << std::endl;
                    return;
                }
                else {
                    while(!wantsMainMenu) characterMenu();
                }

            }
            break;
        }

        case CREATURE: {
            std::cout << std::endl << "-- CREATURE SELECTION --" << std::endl;
            try{
                characters->printAllCharOfType("Creature");
            }
            catch (const std::runtime_error& error){
                std::cout << error.what() << std::endl;
                return;
            }
            auto selected_index = getNumberInput();
            if (selected_index == -1){
                wantsMainMenu = true;
                return;
            }
            else if (selected_index <= 0 || selected_index-1>characters->getMaxIndex("Creature")){
                invalidNumber();
                return;
            }
            else {
                selected_creature = characters->getCreatureCharacter(selected_index-1);
                selected_being = selected_creature;
                selected_type = CRE;
                characterSelected = true;
                if (!characters->containsAny("Creature")){
                    std::cout << "No creatures!" << std::endl;
                    return;
                }
                else {
                    while(!wantsMainMenu) characterMenu();
                }
            }
            break;
        }

        case ELDRITCH: {
            std::cout << std::endl << "-- ELDRITCH HORROR SELECTION --" << std::endl;
            try{
                characters->printAllCharOfType("Eldritch Horror");
            }
            catch (const std::runtime_error& error){
                std::cout << error.what() << std::endl;
                return;
            }
            auto selected_index = getNumberInput();
            if (selected_index == -1){
                wantsMainMenu = true;
                return;
            }
            else if (selected_index <= 0 || selected_index-1>characters->getMaxIndex("Eldritch Horror")){
                invalidNumber();
                return;
            }
            else {
                selected_eldritch = characters->getEldritchCharacter(selected_index-1);
                selected_being = selected_eldritch;
                selected_type = ELD;
                characterSelected = true;
                if (!characters->containsAny("Eldritch Horror")){
                    std::cout << "No eldritch horrors!" << std::endl;
                    return;
                }
                else {
                    while(!wantsMainMenu) characterMenu();
                }
            }
            break;
        }

        case INVESTIGATOR: {
            std::cout << std::endl << "-- INVESTIGATOR SELECTION --" << std::endl;
            try{
                characters->printAllCharOfType("Investigator");
            }
            catch (const std::runtime_error& error){
                std::cout << error.what() << std::endl;
                return;
            }
            auto selected_index = getNumberInput();
            if (selected_index == -1){
                wantsMainMenu = true;
                return;
            }
            else if (selected_index <= 0 || selected_index-1>characters->getMaxIndex("Investigator")){
                invalidNumber();
            }
            else {
                selected_investigator = characters->getInvestigatorCharacter(selected_index-1);
                selected_being = selected_investigator;
                selected_type = INV;
                characterSelected = true;
                if (!characters->containsAny("Investigator")){
                    std::cout << "No investigators!" << std::endl;
                    return;
                }
                else {
                    while(!wantsMainMenu) characterMenu();
                }
            }
            break;
        }

        default:
            invalidNumber();
            break;
    }
}

void Interface::characterMenu() {
    std::cout << std::endl << "-- CHARACTER MENU --" << std::endl;
    mainMenuCommand();
    std::cout << " 1: Edit" << std::endl;
    std::cout << " 2: Delete" << std::endl;
    std::cout << " 3: Print details" << std::endl;
    enum actions { INVALID, MAIN_MENU=-1, EDIT=1, DELETE, PRINT_DETAILS };
    auto action = actions(getNumberInput());
    switch(action){
        case INVALID: {
            invalidNumber();
            break;
        }
        case MAIN_MENU: {
            wantsMainMenu = true;
            return;
        }

        case EDIT: {
            while(!wantsMainMenu) {
                editCharacter();
            }
            break;
        }
        case DELETE: {
            std::cout << "Are you sure you want to delete this character? 1 for yes, 2 for no." << std::endl;
            enum yes_no { INVALID_AF, YES, NO };
            auto answer = yes_no(getNumberInput());

            switch (answer) {
                case INVALID_AF: {
                    std::cout << "Are you retarded?" << std::endl;
                    invalidNumber();
                    break;
                }
                case YES: {
                    deleteCharacter();
                    wantsMainMenu = true;
                    return;
                }
                case NO:
                    std::cout << "Canceling" << std::endl;
                    break;
                default:
                    invalidNumber();
                    break;
            }

            break;
        }
        case PRINT_DETAILS: {
            printCharacter();
            break;
        }
        default: {
            invalidNumber();
            break;
        }
    }
}

void Interface::editCharacter() {
    std::cout << std::endl << "-- EDIT CHARACTER --" << std::endl;
    mainMenuCommand();
    std::cout << " 1: Name" << std::endl;
    std::cout << " 2: Life" << std::endl;
    std::cout << " 3: Strength" << std::endl;
    std::cout << " 4: Intelligence" << std::endl;
    switch (selected_type){
        case NONE:
            std::cout << "Whoopsie" << std::endl;
            break;
        case PERS:
            std::cout << " 5: Fear" << std::endl;
            std::cout << " 6: Gender" << std::endl;
            std::cout << " 7: Make Investigator" << std::endl;
            break;
        case INV:
            std::cout << " 5: Fear" << std::endl;
            std::cout << " 6: Gender" << std::endl;
            std::cout << " 7: Terror" << std::endl;
            break;
        case CRE:
            std::cout << " 5: Naturalness" << std::endl;
            std::cout << " 6: Disquiet" << std::endl;
            break;
        case ELD:
            std::cout << " 5: Traumatism" << std::endl;
            break;
        default:
            std::cout << "what the hell" << std::endl;
            break;
    }

    enum default_attributes { INVALID, MAIN_MENU=-1, NAME=1, LIFE, STRENGTH, INTELLIGENCE };
    int number_choice = getNumberInput();
    auto attr_choice = default_attributes(number_choice);
    switch (attr_choice){
        case INVALID:
            invalidNumber();
        case MAIN_MENU:
            wantsMainMenu = true;
            return;
        case NAME:
            changeName();
            break;
        case LIFE:
            changeLife();
            break;
        case STRENGTH:
            changeStrength();
            break;
        case INTELLIGENCE:
            changeIntelligence();
            break;
        default: {
            switch (selected_type){
                case NONE:
                    invalidNumber();
                    break;
                case PERS:{
                    enum special_attributes { FEAR=5, GENDER=6, MAKE_INVESTIGATOR=7 };
                    auto special = special_attributes(number_choice);
                    switch (special){
                        case FEAR:
                            changePersonFear(false);
                            break;
                        case GENDER:
                            changePersonGender(false);
                            break;
                        case MAKE_INVESTIGATOR:
                            makePersonInvestigator();
                            break;
                        default:
                            invalidNumber();
                            break;
                    }
                    break;
                }
                case INV:{
                    enum special_attributes { FEAR=5, GENDER=6, TERROR=7 };
                    auto special = special_attributes(number_choice);
                    switch (special){
                        case FEAR:
                            changePersonFear(true);
                            break;
                        case GENDER:
                            changePersonGender(true);
                            break;
                        case TERROR:
                            changeInvestigatorTerror();
                        default:
                            invalidNumber();
                            break;
                    }
                    break;
                }
                case CRE:{
                    enum special_attributes { NATURALNESS=5, DISQUIET=6 };
                    auto special = special_attributes(number_choice);
                    switch (special){
                        case NATURALNESS:
                            changeCreatureNaturalness();
                            break;
                        case DISQUIET:
                            changeCreatureDisquiet();
                            break;
                        default:
                            invalidNumber();
                            break;
                    }
                    break;
                }

                case ELD:{
                    enum special_attributes { TRAUMATISM=5 };
                    auto special = special_attributes(number_choice);
                    if (special == TRAUMATISM) changeEldritchTraumatism();
                    else invalidNumber();
                    break;
                }
                default:
                    invalidNumber();
                    break;
            }
        }
    }
}

void Interface::load() {
    std::cout << std::endl << "-- LOAD MENU --" << std::endl;
    std::cout << "Enter name of roster to load (WITHOUT FILE ENDING)" << std::endl;
    std::string filename;
    std::cin >> filename;
    std::string temp_filename = filename+".json";
    try {
        std::ifstream in;
        in.open(temp_filename);
        if (!in.is_open()){
            std::cout << "File does not exist!" << std::endl;
            wantsMainMenu = true;
            return;
        }
        else {
            roles->reset();
            characters->reset();
            roles->load(filename);
            characters->load(filename);
            std::cout << "Roster loaded!" << std::endl;
            wantsMainMenu = true;
            return;
        }
    }
    catch (const std::runtime_error& error){
        std::cout << "File not found! :(" << std::endl;
        wantsMainMenu = true;
        return;
    }
}

void Interface::save() {
    std::cout << std::endl << "-- SAVE MENU --" << std::endl;
    std::cout << "Enter file to save roster to (WITHOUT FILE ENDING)" << std::endl;
    std::string filename = getStringInput();
    characters->save(filename); // saves role file automatically
}

void Interface::deleteCharacter() {
    switch(selected_type){
        case PERS:{
            if (!roles->roleExists("Person", selected_being->getRole())){
                std::cout << "Role not found for type :(" << std::endl;
            }
            characters->removeCharacter(selected_person->getType(),
                                        selected_person->getRole(),
                                        selected_person->getName());
            return;
        }
        case INV:{
            if (!roles->roleExists("Person", selected_being->getRole())){
                std::cout << "Role not found for type :(" << std::endl;
            }
            characters->removeCharacter(selected_investigator->getType(),
                                        selected_investigator->getRole(),
                                        selected_investigator->getName());
            return;
        }
        case CRE:{
            if (!roles->roleExists("Creature", selected_being->getRole())){
                std::cout << "Role not found for type :(" << std::endl;
            }
            characters->removeCharacter(selected_creature->getType(),
                                        selected_creature->getRole(),
                                        selected_creature->getName());
            return;
        }
        case ELD:{
            if (!roles->roleExists("Eldritch Horror", selected_being->getRole())){
                std::cout << "Role not found for type :(" << std::endl;
            }
            characters->removeCharacter(selected_eldritch->getType(),
                                        selected_eldritch->getRole(),
                                        selected_eldritch->getName());
            return;
        }
        case NONE:{
            std::cout<< "NOTHING TO DELETE" << std::endl;
            return;
        }
    }

    characters->removeCharacter(selected_being->getType(),
                                selected_being->getRole(),
                                selected_being->getName());
    std::cout << "Character deleted!" << std::endl;
}

void Interface::createCharacter() {
    std::cout << std::endl << "-- CREATE CHARACTER --" << std::endl;
    mainMenuCommand();
    roles->printTypes();
    enum creation_types { INVALID, MAIN_MENU=-1, INVESTIGATOR=1, PERSON, CREATURE, ELDRITCH_HORROR };
    auto wished_type = creation_types(getNumberInput());
    switch (wished_type){
        case INVALID:{
            invalidNumber();
            break;
        }
        case MAIN_MENU:{
            wantsMainMenu = true;
            return;
        }
        case INVESTIGATOR:{
            std::cout << std::endl << "-- ROLES --" << std::endl;
            roles->printTypeRoles("Investigator");
            createPersonCharacter(true);
            break;
        }
        case PERSON:{
            std::cout << std::endl << "-- ROLES --" << std::endl;
            roles->printTypeRoles("Person");
            createPersonCharacter(false);
            break;
        }
        case CREATURE:{
            std::cout << std::endl << "-- ROLES --" << std::endl;
            roles->printTypeRoles("Creature");
            createCreatureCharacter();
            break;
        }
        case ELDRITCH_HORROR:{
            std::cout << std::endl << "-- ROLES --" << std::endl;
            roles->printTypeRoles("Eldritch Horror");
            createEldritchCharacter();
            break;
        }
        default:{
            invalidNumber();
            break;
        }
    }

}

void Interface::createRole() {
    std::cout << std::endl << "-- CREATE ROLE --" << std::endl;
    mainMenuCommand();
    std::cout << " 1: Person" << std::endl;
    std::cout << " 2: Creature" << std::endl;
    std::cout << " 3: Eldritch Horror" << std::endl;

    enum types { INVALID, MAIN_MENU=-1, PERSON=1, CREATURE, ELDRITCH };
    auto action = types(getNumberInput());
    switch (action){
        case INVALID:{
            invalidNumber();
            break;
        }
        case MAIN_MENU:{
            wantsMainMenu = true;
            return;
        }
        case PERSON:{
            role_being::loader* being_data = getBeingFromUser("Person");
            role_person::loader* person_data = getPersonFromUser();
            roles->createPersonRole(*being_data, *person_data);
            std::cout << std::endl << "Role created!" << std::endl;
            break;
        }
        case CREATURE:{
            role_being::loader* being_data = getBeingFromUser("Creature");
            role_creature::loader* creature_data = getCreatureFromUser();
            roles->createCreatureRole(*being_data, *creature_data);
            std::cout << std::endl << "Role created!" << std::endl;
            break;
        }
        case ELDRITCH:{
            role_being::loader* being_data = getBeingFromUser("Eldritch Horror");
            role_creature::loader* creature_data = getCreatureFromUser();
            role_eldritch::loader* eldritch_data = getEldritchFromUser();
            roles->createEldritchRole(*being_data, *creature_data, *eldritch_data);
            std::cout << std::endl << "Role created!" << std::endl;
            break;
        }
        default:{
            invalidNumber();
            break;
        }
    }
}

void Interface::deleteRole() {
    std::cout << std::endl << "-- DELETE ROLE --" << std::endl;
    mainMenuCommand();
    std::cout << " 1: Person" << std::endl;
    std::cout << " 2: Creature" << std::endl;
    std::cout << " 3: Eldritch Horror" << std::endl;
    enum types { INVALID, MAIN_MENU=-1, PERSON=1, CREATURE, ELDRITCH };
    auto action = types(getNumberInput());
    switch (action){
        case INVALID:
            invalidNumber();
            break;
        case MAIN_MENU:
            wantsMainMenu = true;
            return;
        case PERSON: {
            roles->printTypeRoles("Person");
            std::cout << std::endl;
            std::cout << "Enter NAME of role to delete" << std::endl;
            std::string role_to_delete = getStringInput();
            if (roles->roleExists("Person", role_to_delete)){
                characters->removeAllCharactersOfRole("Person", role_to_delete);
                roles->removeRole("Person", role_to_delete);
            }
            else {
                std::cout << "This role does not exists for persons." << std::endl;
            }
            break;
        }
        case CREATURE: {
            roles->printTypeRoles("Creature");
            std::cout << std::endl;
            std::cout << "Enter NAME of role to delete" << std::endl;
            std::string role_to_delete = getStringInput();
            if (roles->roleExists("Creature", role_to_delete)){
                characters->removeAllCharactersOfRole("Creature", role_to_delete);
                roles->removeRole("Creature", role_to_delete);
            }
            else {
                std::cout << "This role does not exists for creatures." << std::endl;
            }
            break;
        }
        case ELDRITCH:{
            roles->printTypeRoles("Eldritch Horror");
            std::cout << std::endl;
            std::cout << "Enter NAME of role to delete" << std::endl;
            std::string role_to_delete = getStringInput();
            if (roles->roleExists("Eldritch Horror", role_to_delete)){
                characters->removeAllCharactersOfRole("Eldritch Horror", role_to_delete);
                roles->removeRole("Eldritch Horror", role_to_delete);
            }
            else {
                std::cout << "This role does not exists for eldritch horror." << std::endl;
            }
            break;
        }
        default:{
            invalidNumber();
            break;
        }
    }
}

void Interface::invalidNumber() {
    std::cout << "Invalid number." << std::endl;
}

void Interface::print() {
    std::cout << std::endl << "-- PRINT MENU --" << std::endl;
    mainMenuCommand();
    std::cout << " 1: Print roster" << std::endl;
    std::cout << " 2: Print roles" << std::endl;
    enum what_to_print { INVALID, MAIN_MENU=-1, ROSTER=1, ROLES };
    auto choice = what_to_print(getNumberInput());
    switch (choice){
        case INVALID:
            invalidNumber();
            break;
        case MAIN_MENU:
            wantsMainMenu = true;
            return;
        case ROSTER: {
            std::cout << "Print roster options" << std::endl;
            mainMenuCommand();
            std::cout << " 1: Everyone" << std::endl;
            std::cout << " 2: Investigators" << std::endl;
            std::cout << " 3: Persons" << std::endl;
            std::cout << " 4: Creatures" << std::endl;
            std::cout << " 5: Eldritch Horrors" << std::endl;

            enum options { INVALID_OPTION, MAIN=-1, EVERYONE=1, INVESTIGATOR, PERSON, CREATURE, ELDRITCH };
            auto selected_option = options(getNumberInput());
            switch (selected_option){
                case INVALID_OPTION:
                    invalidNumber();
                    break;
                case MAIN:
                    wantsMainMenu = true;
                    return;
                case EVERYONE:
                    characters->printAllCharDetails();
                    break;
                case INVESTIGATOR:
                    characters->printAllCharOfType("Investigator");
                    break;
                case PERSON:
                    characters->printAllCharOfType("Person");
                    break;
                case CREATURE:
                    characters->printAllCharOfType("Creature");
                    break;
                case ELDRITCH:
                    characters->printAllCharOfType("Eldritch Horror");
                    break;
                default:
                    invalidNumber();
                    break;
            }
            break;
        }
        case ROLES:{
            roles->printAllRoleDetails();
            break;
        }
        default:{
            invalidNumber();
            break;
        }
    }
}

int Interface::getNumberInput() {
    int num;
    std::cin >> num;
    return num;
}

std::string Interface::getStringInput() {
    std::string str;
    std::cin >> str;
    return str;
}

void Interface::mainMenuCommand() {
    std::cout << "-1: Main menu" << std::endl;
}

void Interface::printCharacter() {
    switch (selected_type){
        case NONE:
            std::cout << "You should not even be here!" << std::endl;
            break;
        case PERS:
            selected_person->printAttributes();
            break;
        case INV:
            selected_investigator->printAttributes();
            break;
        case CRE:
            selected_creature->printAttributes();
            break;
        case ELD:
            selected_eldritch->printAttributes();
            break;
        default:
            std::cout << "You should not even be here!" << std::endl;
            break;
    }
}

void Interface::changeName() {
    std::cout << "Enter new name: " << std::endl;
    std::string name;
    std::cin >> name;
    selected_being->changeName(name);
}

void Interface::changeLife() {
    std::cout << "Enter new life: " << std::endl;
    selected_being->printMaxMinLife();
    int life = getNumberInput();
    selected_being->changeLife(life);
}

void Interface::changeStrength() {
    std::cout << "Enter new strength: " << std::endl;
    selected_being->printMaxMinStrength();
    int strength = getNumberInput();
    selected_being->changeStrength(strength);
}

void Interface::changeIntelligence() {
    std::cout << "Enter new intelligence: " << std::endl;
    selected_being->printMaxMinIntelligence();
    int intelligence = getNumberInput();
    selected_being->changeIntelligence(intelligence);
}

void Interface::changePersonFear(bool isInvestigator) {
    std::cout << "Enter new fear value" << std::endl;
    int fear = getNumberInput();
    if (isInvestigator){
        selected_investigator->changeFear(fear);
        return;
    }
    selected_person->changeFear(fear);
}

void Interface::changePersonGender(bool isInvestigator) {
    std::cout << "Enter new gender" << std::endl;
    std::string gender = getStringInput();
    if (isInvestigator){
        selected_investigator->changeGender(gender);
        return;
    }
    selected_person->changeGender(gender);
}

void Interface::changeInvestigatorTerror() {
    std::cout << "Enter new terror value" << std::endl;
    int terror = getNumberInput();
    selected_investigator->changeTerror(terror);
}

void Interface::changeCreatureNaturalness() {
    selected_creature->changeNaturalness();
}

void Interface::changeCreatureDisquiet() {
    std::cout << "Enter new disquiet value" << std::endl;
    int disquiet = getNumberInput();
    selected_creature->changeDisquiet(disquiet);
}

void Interface::changeEldritchTraumatism() {
    std::cout << "Enter new traumatism value" << std::endl;
    int traumatism = getNumberInput();
    selected_eldritch->changeTraumatism(traumatism);
}

void Interface::createPersonCharacter(bool isInvestigator) {
    std::string role;
    try {
        role = getRoleFromUser("Person");
    }
    catch(const std::runtime_error& error) {
        std::cout << "This role does not exist" << std::endl;
        return;
    }

    std::cout << "Enter a name for your new character" << std::endl;
    std::string name = getStringInput();
    std::cout << "Enter a gender for your new character" << std::endl;
    std::string gender = getStringInput();

    Person* new_person = roles->createPerson(role, name, gender);
    if (isInvestigator){
        new_person->makeInvestigator();
        selected_investigator = new_person;
        selected_type = INV;
    }
    else {
        selected_person = new_person;
        selected_type = PERS;
    }
    selected_being = new_person;
    characters->add(new_person);
    std::cout << "Character created!" << std::endl;
}

void Interface::createCreatureCharacter() {
    std::string role;
    try {
        role = getRoleFromUser("Creature");
    }
    catch(const std::runtime_error& error) {
        std::cout << "This role does not exist" << std::endl;
        return;
    }
    Creature* new_creature = roles->createCreature(role);
    selected_type = CRE;
    selected_creature = new_creature;
    selected_being = new_creature;
    std::cout << "Character created!" << std::endl;
}

void Interface::createEldritchCharacter() {
    std::string role;
    try {
        role = getRoleFromUser("Eldritch Horror");
    }
    catch(const std::runtime_error& error) {
        std::cout << "This role does not exist" << std::endl;
        return;
    }
    EldritchHorror* new_eldritch = roles->createEldritch(role);
    selected_type = ELD;
    selected_eldritch = new_eldritch;
    selected_being = new_eldritch;
    std::cout << "Character created!" << std::endl;
}

void Interface::makePersonInvestigator() {
    selected_person->makeInvestigator();
}

std::string Interface::getRoleFromUser(const string &type) {
    int index = getNumberInput();
    return roles->getRole(type, index);
}

role_being::loader *Interface::getBeingFromUser(const std::string& type) {
    auto* new_stats = new role_being::loader;
    new_stats->Type = type;
    std::cout << "Enter minimum life" << std::endl;
    new_stats->MinLife = getNumberInput();
    std::cout << "Enter maximum life" << std::endl;
    new_stats->MaxLife = getNumberInput();
    std::cout << "Enter minimum strength" << std::endl;
    new_stats->MinStrength = getNumberInput();
    std::cout << "Enter maximum strength" << std::endl;
    new_stats->MaxStrength = getNumberInput();
    std::cout << "Enter minimum intelligence" << std::endl;
    new_stats->MinIntelligence = getNumberInput();
    std::cout << "Enter maximum intelligence" << std::endl;
    new_stats->MaxIntelligence = getNumberInput();

    return new_stats;
}

role_person::loader *Interface::getPersonFromUser() {
    auto* new_stats = new role_person::loader;
    std::cout << "Enter the role name for your person" << std::endl;
    std::string role = getStringInput();
    new_stats->Role = role;
    return new_stats;
}

role_eldritch::loader *Interface::getEldritchFromUser() {
    auto* new_stats = new role_eldritch::loader;
    std::cout << "Enter traumatism for your eldritch role" << std::endl;
    new_stats->Traumatism = getNumberInput();
    return new_stats;
}

role_creature::loader *Interface::getCreatureFromUser() {
    auto* new_stats = new role_creature::loader;
    std::cout << "Enter the role name for your person" << std::endl;
    std::string role = getStringInput();
    new_stats->Role = role;

    std::cout << "Is the creature unnatural? Enter 1 for yes, Enter 0 for no.";
    bool isUnnatural;
    std::cin >> isUnnatural;
    new_stats->Unnatural = isUnnatural;

    std::cout << "Enter disquiet value" << std::endl;
    new_stats->Disquiet = getNumberInput();
    return new_stats;
}




















