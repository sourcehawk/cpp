#ifndef HW4_BOOLATTRIBUTE_H
#define HW4_BOOLATTRIBUTE_H
#include "Attribute.h"
using json = nlohmann::json;


class BoolAttribute: public Attribute{
public:
    BoolAttribute(const string& name, bool& value): value(value), Attribute(name){

    }
    void printAttribute() override{
        Attribute::printAttributeName();
        if (value){
            cout << "true" << endl;
        }
        else {
            cout << "false" << endl;
        }
    }
    // Returning json allows us to return any type from the same method
    json value_to_json() override{
        json j_bool = value;
        return j_bool;
    }

    bool& value;
};

#endif //HW4_BOOLATTRIBUTE_H
