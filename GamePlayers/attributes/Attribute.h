#ifndef HW4_ATTRIBUTE_H
#define HW4_ATTRIBUTE_H
#include "../json.hpp"
#include <iostream>
using namespace std;
using json = nlohmann::json;
/*
 * This class is intended to maintain all attributes of a class. It allows for easy access
 * to all class attributes, even the ones not directly accessible through parent class instantiation of a subclass.
 * (That requires maintaining the attribute classes in a linked list, as done in the Being class)
 *
 * Attribute class is the base class for the int, string and bool attribute classes.
 * Each of these classes have a variable **name** and **value**.
 *
 * The subclasses of the Attribute class receive a reference to a variable to maintain as their value.
 * When these variables are changed outside of the attribute class they will
 * automatically change within the attribute class as well.
 *
 *
 * Note: Attribute names are not references and should not be changed after initialization.
 *
 * Usage demonstration:
 * Here we are changing the attribute's value without direct access.

    CODE:
    // Initialize attribute
    int my_strength = 10;

    // Note that the instantiation is through the base class
    Attribute* int_attribute = new IntAttribute("Strength", my_strength);
    int_attribute->printAttribute();

    // Change the variable
    my_strength = 5;
    int_attribute->printAttribute();

    OUTPUT:
    Strength: 10
    Strength: 5
 */
class Attribute {
public:
    explicit Attribute(const string& name){
        this->name = name;
    }
    string name;

    void printAttributeName() const{
        cout << name << ": ";
    }
    virtual void printAttribute(){
        cout << "Feature not implemented." << endl;
    }
    virtual json value_to_json(){
        throw runtime_error("Not implemented");
    }
    json name_to_json(){
        json j_name = name;
        return j_name;
    }
};


#endif //HW4_ATTRIBUTE_H