#ifndef HW4_INTATTRIBUTE_H
#define HW4_INTATTRIBUTE_H
#include "Attribute.h"
#include "../json.hpp"
using json = nlohmann::json;


class IntAttribute: public Attribute {
public:
    IntAttribute(const string& name, int& value): value(value), Attribute(name){
        this->value = value;
    }
    void printAttribute() override{
        Attribute::printAttributeName();
        cout << value << endl;
    }
    json value_to_json() override{
        json j_int = value;
        return j_int;
    }

    int& value;
};

#endif //HW4_INTATTRIBUTE_H
