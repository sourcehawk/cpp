#ifndef HW4_NODE_H
#define HW4_NODE_H
using namespace std;

template <class T>
struct Node {
    explicit Node(T data){
        this->data = data;
        this->next = nullptr;
    };
    T data;
    Node<T> *next;
};

#endif //HW4_NODE_H
