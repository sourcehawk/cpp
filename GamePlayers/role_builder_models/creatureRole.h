#ifndef HW4_CREATUREROLE_H
#define HW4_CREATUREROLE_H
#include "../json.hpp"
using json = nlohmann::json;


namespace role_creature {
    struct loader {
        std::string Role;
        int Disquiet;
        bool Unnatural;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(loader, Role, Disquiet, Unnatural)
}

#endif //HW4_CREATUREROLE_H
