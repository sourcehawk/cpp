#ifndef HW4_PERSONROLE_H
#define HW4_PERSONROLE_H
#include "../json.hpp"
using json = nlohmann::json;

// If type == investigator then load person
namespace role_person {
    struct loader {
        std::string Role;
        // Fear is random
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(loader, Role)
}

#endif //HW4_PERSONROLE_H
