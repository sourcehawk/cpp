#include "Characters.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include "../role_builder_models/beingRole.h"
using json = nlohmann::json;

Characters::Characters(Roles& roles, const std::string &filename): roles(roles) {
    load(filename);
}

Characters::Characters(Roles &roles): roles(roles) {
    // do nothing :)
}

Person *Characters::create_person_character(json &j, char_being::loader &being_data) {
    char_person::loader person_data = j.get<char_person::loader>();
    auto* person = new Person(being_data, person_data);
    return person;
}

Creature *Characters::create_creature_character(json &j, char_being::loader &being_data) {
    char_creature::loader creature_data = j.get<char_creature::loader>();
    auto* creature = new Creature(being_data, creature_data);
    return creature;
}

EldritchHorror *Characters::create_eldritch_character(json &j, char_being::loader &being_data) {
    char_creature::loader creature_data = j.get<char_creature::loader>();
    char_eldritch::loader eldritch_data = j.get<char_eldritch::loader>();
    auto* eldritch = new EldritchHorror(being_data, creature_data, eldritch_data);
    return eldritch;
}

void Characters::add(Person *person) {
    person_characters.push_back(person);
    if (person->investigator()) investigator_characters.push_back(person);
    beings.push_back(person);
}

void Characters::add(Creature *creature) {
    creature_characters.push_back(creature);
    beings.push_back(creature);
}

void Characters::add(EldritchHorror *eldritch) {
    eldritch_characters.push_back(eldritch);
    beings.push_back(eldritch);
}

void Characters::save(std::string filename) {
    std::string new_filename = filename+".json";
    json character_array = json::array();
    for (auto* being: beings){
        json curr_char;
        being->to_json(curr_char);
        character_array.push_back(curr_char);
    }
    std::ofstream out;
    out.open(new_filename);
    out << std::setw(4) << character_array << std::endl;
    out.close();
    roles.save(filename);
}

void Characters::addToRoleCount(const std::string &role) {
    if (role_exists(role)) role_counter.insert(std::make_pair(role, 1));
    else role_counter[role]++;
}

std::string Characters::getNPCname(const std::string &role) {
    addToRoleCount(role);
    int current_count = role_counter[role];
    std::string number = std::to_string(current_count);
    std::string npc_name = role + " " + number;
    return npc_name;
}

void Characters::printAllCharDetails() {
    for (auto* being: beings){
        being->printAttributes();
    }
}

void Characters::printAllCharOfType(const std::string &typeName) {
    if (typeName == "Creature"){
        int i=1;
        for(auto* being: creature_characters){
            std::cout << i << ": " << being->getName() << std::endl;
            i++;
        }
        if (i == 1) throw std::runtime_error("No creatures!");
    }
    else if (typeName== "Eldritch Horror"){
        int i=1;
        for(auto* being: eldritch_characters){
            std::cout << i << ": " << being->getName() << std::endl;
            i++;
        }
        if (i == 1) throw std::runtime_error("No eldritch horror characters!");
    }
    else if (typeName == "Person"){
        int i=1;
        for(auto* being: person_characters){
            if (being->getType() != "Investigator"){
                std::cout << i << ": " << being->getName() << std::endl;
                i++;
            }
        }
        if (i == 1) throw std::runtime_error("No persons!");
    }
    else if (typeName == "Investigator"){
        int i=1;
        for(auto* being: investigator_characters){
            std::cout << i << ": " << being->getName() << std::endl;
            i++;
        }
        if (i == 1) throw std::runtime_error("No investigators!");
    }

}

void Characters::removeCharacter(const std::string& type, const std::string& roleName, const std::string& name) {
    bool removed = false;
    int index = 0;
    if (type == "Investigator"){
        for (auto* investigator: investigator_characters){
            if (investigator->getRole() == roleName && investigator->getName() == name && investigator->investigator()){
                removed = true;
                investigator_characters.erase(investigator_characters.begin()+index);
            }
            index++;
        }
    }
    if (type == "Person" || type == "Investigator"){
        index = 0;
        for (auto* person: person_characters){
            if (person->getRole() == roleName && person->getName() == name){
                removed = true;
                person_characters.erase(person_characters.begin()+index);
            }
            index++;
        }
    }
    else if (type == "Creature"){
        for (auto* creature: creature_characters){
            if (creature->getRole() == roleName && creature->getName() == name){
                removed = true;
                creature_characters.erase(creature_characters.begin()+index);
            }
            index++;
        }
    }
    else if (type == "Eldritch Horror"){
        for (auto* eldritch: eldritch_characters){
            if (eldritch->getRole() == roleName && eldritch->getName() == name){
                removed = true;
                eldritch_characters.erase(eldritch_characters.begin()+index);
            }
            index++;
        }
    }
    int being_index = 0;
    for (auto* being: beings){
        if (being->getName() == name && being->getRole() == roleName && being->getType() == type){
            Being* delete_begin = being;
            beings.erase(beings.begin()+being_index);
            delete delete_begin;
        }
        being_index++;
    }
    if (removed) std::cout << "Successfully removed." << std::endl;
    else std::cout << "Removal unsuccessful. Character not found with given data." << std::endl;
}

bool Characters::role_exists(const string &roleName) {
    int cnt = role_counter.count(roleName);
    if (cnt == 0) return false;
    return true;
}


void Characters::reset() {
    role_counter.erase(role_counter.begin(), role_counter.end());
    for (auto* being: beings){
        delete being; // Deletes the objects in all the other vectors too
    }
    beings.resize(0);
    person_characters.resize(0);
    investigator_characters.resize(0);
    creature_characters.resize(0);
    eldritch_characters.resize(0);
}

EldritchHorror *Characters::getEldritchCharacter(int index) {
    return eldritch_characters.at(index);
}

Creature *Characters::getCreatureCharacter(int index) {
    return creature_characters.at(index);
}

Person *Characters::getPersonCharacter(int index) {
    return person_characters.at(index);
}

Person *Characters::getInvestigatorCharacter(int index) {
    return investigator_characters.at(index);
}

int Characters::getMaxIndex(const string &type) {
    if (type == "Creature"){
        return creature_characters.size();
    }
    else if (type == "Person"){
        return person_characters.size();
    }
    else if (type == "Investigator"){
        return investigator_characters.size();
    }
    else if (type == "Eldritch Horror"){
        return eldritch_characters.size();
    }
    else {
        throw std::runtime_error("Type does not exist");
    }
}

void Characters::load(std::string filename) {
    filename+=".json";
    json json_characters;
    std::ifstream in;
    in.open(filename);
    if (!in.is_open()){
        throw std::runtime_error("File not found.");
    }

    // Json array of roles
    in >> json_characters;
    for (auto& elem: json_characters){
        auto being_data = elem.get<char_being::loader>();
        if (being_data.Type == "Person" || being_data.Type == "Investigator") {
            Person* pers = create_person_character(elem, being_data);
            role_being::loader* role_settings = roles.getRoleSettings(being_data.Type, pers->getRole());
            pers->loadCharacterSettings(*role_settings);
            delete role_settings;
            person_characters.push_back(pers);
            beings.push_back(pers);
            if (being_data.Type == "Investigator") investigator_characters.push_back(pers);
        }
        else if (being_data.Type == "Creature"){
            Creature* cret = create_creature_character(elem, being_data);
            role_being::loader* role_settings = roles.getRoleSettings(being_data.Type, cret->getRole());
            cret->loadCharacterSettings(*role_settings);
            delete role_settings;
            if (!cret->named()) cret->changeNPCname(getNPCname(cret->getRole()));
            creature_characters.push_back(cret);
            beings.push_back(cret);
        }
        else if (being_data.Type == "Eldritch Horror"){
            EldritchHorror* eldr = create_eldritch_character(elem, being_data);
            role_being::loader* role_settings = roles.getRoleSettings(being_data.Type, eldr->getRole());
            eldr->loadCharacterSettings(*role_settings);
            delete role_settings;
            if (!eldr->named()) eldr->changeNPCname(getNPCname(eldr->getRole()));
            creature_characters.push_back(eldr);
            beings.push_back(eldr);
        }
        else throw std::runtime_error("Unrecognized type declared in character loading file.");
    }
    in.close();
}

void Characters::removeAllCharactersOfRole(const string &type, const string &roleName) {
    int idx = 0;
    if (type == "Person"){
        for (auto person: person_characters){
            if (person->getRole() == roleName){
                delete person;
                person_characters.erase(person_characters.begin()+idx);
            }
            idx++;
        }
    }
    else if (type == "Creature"){
        for (auto creature: creature_characters){
            if (creature->getRole() == roleName){
                delete creature;
                creature_characters.erase(creature_characters.begin()+idx);
            }
            idx++;
        }
    }
    else if (type == "Eldritch Horror"){
        for (auto eldritch: eldritch_characters){
            if (eldritch->getRole() == roleName){
                delete eldritch;
                eldritch_characters.erase(eldritch_characters.begin()+idx);
            }
            idx++;
        }
    }
    else {
        std::cout << "Type not recognized." << std::endl;
    }

}

bool Characters::containsAny(const string &type) {
    if (type == "Person"){
        int cnt = 0;
        for (auto* person: person_characters){
            if (!person->investigator()){
                return true;
            }
        }
        return false;
    }
    else if (type == "Creature"){
        return !creature_characters.empty();
    }
    else if (type == "Eldritch Horror"){
        return !eldritch_characters.empty();
    }
    else if (type == "Investigator"){
        return !investigator_characters.empty();
    }
    else {
        std::cout << "Type not recognized." << std::endl;
    }
    return false;
}

