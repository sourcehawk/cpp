#include "Roles.h"
#include <fstream>
#include <iomanip>
#include <iostream>
#include <stdexcept>
using json = nlohmann::json;

// Load roles from file
Roles::Roles(const std::string& filename) {
    load(filename);
}

// Construct empty class
Roles::Roles() = default;

// Reset to empty class
void Roles::reset() {
    for (auto being: beings){
        delete being; // This will delete all pointers in the maps
    }
    beings.resize(0);
    creature_roles.erase(creature_roles.begin(), creature_roles.end());
    person_roles.erase(person_roles.begin(), person_roles.end());
    eldritch_roles.erase(eldritch_roles.begin(), eldritch_roles.end());
}

// Internal method for loading roles from file
Person *Roles::create_person_role(json &j, role_being::loader &being_data) {
    role_person::loader person_data = j.get<role_person::loader>();
    auto* person = new Person(being_data, person_data);
    return person;
}
// Internal method for loading roles from file
Creature *Roles::create_creature_role(json &j, role_being::loader &being_data) {
    role_creature::loader creature_data = j.get<role_creature::loader>();
    auto* creature = new Creature(being_data, creature_data);
    return creature;
}
// Internal method for loading roles from file
EldritchHorror *Roles::create_eldritch_role(json &j, role_being::loader &being_data) {
    role_creature::loader creature_data = j.get<role_creature::loader>();
    role_eldritch::loader eldritch_data = j.get<role_eldritch::loader>();
    auto* eldritch = new EldritchHorror(being_data, creature_data, eldritch_data);
    return eldritch;
}
// External character creation
Person *Roles::createPerson(const std::string &roleName, const std::string &name, const std::string &gender) {
    int cnt = person_roles.count(roleName);
    if (cnt==0) {
        std::cout << "Role does not exist for this type" << std::endl;
        return nullptr;
    }
    Person* person = person_roles[roleName];
    role_being::loader* role_settings = person->getRoleSettings();
    auto* new_person = new Person(*person->getBeingData(), *person->getPersonData());
    new_person->loadRoleSettings(*role_settings);
    new_person->changeName(name);
    new_person->changeGender(gender);
    delete role_settings;
    return new_person;
}
// External character creation
Creature *Roles::createCreature(const std::string &roleName) {
    int cnt = creature_roles.count(roleName);
    if (cnt==0) {
        std::cout << "Role does not exist for this type" << std::endl;
        return nullptr;
    }
    Creature* creature = creature_roles[roleName];
    role_being::loader* role_settings = creature->getRoleSettings();
    auto* new_creature = new Creature(*creature->getBeingData(), *creature->getCreatureData());
    new_creature->loadRoleSettings(*role_settings);
    delete role_settings;
    return new_creature;
}
// External character creation
EldritchHorror *Roles::createEldritch(const std::string &roleName) {
    int cnt = eldritch_roles.count(roleName);
    if (cnt==0) {
        std::cout << "Role does not exist for this type" << std::endl;
        return nullptr;
    }
    EldritchHorror* eldritchHorror = eldritch_roles[roleName];
    role_being::loader* role_settings = eldritchHorror->getRoleSettings();
    auto* new_eldritch = new EldritchHorror(*eldritchHorror->getBeingData(), *eldritchHorror->getCreatureData(),
                                            *eldritchHorror->getEldritchData());
    new_eldritch->loadRoleSettings(*role_settings);
    delete role_settings;
    return new_eldritch;
}

void Roles::printRoles() {
    int i=1;
    for(auto* being: beings){
        std::cout << i << ": " << being->getRole() << std::endl;
        i++;
    }
}
void Roles::printAllRoleDetails() {
    for (auto* being: beings){
        being->printAttributes();
    }
}
void Roles::printRoleDetails(const std::string &roleName) {
    bool found = false;
    for (auto* being: beings){
        if (being->getRole() == roleName){
            being->printAttributes();
            found = true;
            break;
        }
    }
    if (!found) std::cout << "Role not found!" << std::endl;
}

// External role creation
void Roles::createPersonRole(role_being::loader &being_data, role_person::loader &person_data) {
    if (roleExists(being_data.Type, person_data.Role)) {
        std::cout << "This role already exists!" << std::endl;
        return;
    }
    auto* new_person = new Person(being_data, person_data);
    person_roles.insert(std::make_pair(person_data.Role, new_person));
    beings.push_back(new_person);
}
// External role creation
void Roles::createCreatureRole(role_being::loader &being_data, role_creature::loader &creature_data) {
    if (roleExists(being_data.Type, creature_data.Role)) {
        std::cout << "This role already exists!" << std::endl;
        return;
    }
    auto* new_creature = new Creature(being_data, creature_data);
    creature_roles.insert(std::make_pair(creature_data.Role, new_creature));
    beings.push_back(new_creature);
}
// External role creation
void Roles::createEldritchRole(role_being::loader &being_data, role_creature::loader &creature_data,
                               role_eldritch::loader &eldritch_data) {
    if (roleExists(being_data.Type, creature_data.Role)) {
        std::cout << "This role already exists!" << std::endl;
        return;
    }
    auto* new_eldritch = new EldritchHorror(being_data, creature_data, eldritch_data);
    eldritch_roles.insert(std::make_pair(creature_data.Role, new_eldritch));
    beings.push_back(new_eldritch);
}

void Roles::removeRole(const std::string& type, const std::string& roleName) {
    if (type == "Person" || type == "Investigator"){
        std::map<std::string, Person*>::iterator it;
        it = person_roles.find(roleName);
        delete it->second;
        if (it != person_roles.end()) person_roles.erase(it);
    }
    else if (type == "Creature"){
        std::map<std::string, Creature*>::iterator it;
        it = creature_roles.find(roleName);
        delete it->second;
        if (it != creature_roles.end()) creature_roles.erase(it);
    }
    else if (type == "Eldritch Horror"){
        std::map<std::string, EldritchHorror*>::iterator it;
        it = eldritch_roles.find(roleName);
        delete it->second;
        if (it != eldritch_roles.end()) eldritch_roles.erase(it);
    }
    else {
        std::cout << "Type for role removal not found!" << std::endl;
        return;
    }
    int idx = 0;
    for (auto* being: beings){
        if (being->getRole() == roleName && being->getType() == type){
            delete being;
            beings.erase(beings.begin()+idx);
        }
        idx++;
    }
    std::cout << "Role deleted" << std::endl;
}

void Roles::save(std::string filename) {
    filename+="_roles.json";
    json role_array = json::array();
    for (auto* being: beings){
        json curr_char;
        being->to_json(curr_char);
        role_array.push_back(curr_char);
    }
    std::ofstream out;
    out.open(filename);
    out << std::setw(4) << role_array << std::endl;
    out.close();
}

bool Roles::roleExists(const string &type, const string &roleName) {
    int cnt;
    if (type == "Creature"){
        cnt = creature_roles.count(roleName);
    }
    else if (type == "Person" || type == "Investigator"){
        cnt = person_roles.count(roleName);
    }
    else if (type == "Eldritch Horror"){
        cnt = eldritch_roles.count(roleName);
    }
    else {
        throw std::runtime_error("Type does not exist");
    }
    if (cnt == 0) {
        return false;
    }
    return true;
}

role_being::loader *Roles::getRoleSettings(const string &type, const string &roleName) {
    if (!roleExists(type, roleName)) throw "This role does not exist. (Roles.cpp: getRoleSettings";
    if (type == "Investigator" || type == "Person"){
        return person_roles[roleName]->getRoleSettings();
    }
    else if (type == "Creature"){
        return creature_roles[roleName]->getRoleSettings();
    }
    else if (type == "Eldritch Horror"){
        return eldritch_roles[roleName]->getRoleSettings();
    }
    else {
        throw std::runtime_error("This role does not exist. (getRoleSettings)");
    }
}

void Roles::load(std::string filename) {
    filename+="_roles.json";
    json json_roles;
    std::ifstream in;
    in.open(filename);
    if (!in.is_open()){
        throw std::runtime_error("File not found.");
    }

    // Json array of roles
    in >> json_roles;
    for (auto& elem: json_roles){
        auto being_data = elem.get<role_being::loader>();
        if (being_data.Type == "Person") {
            Person* pers = create_person_role(elem, being_data);
            person_roles.insert(std::make_pair(pers->getRole(), pers));
            beings.push_back(pers);
        }
        else if (being_data.Type == "Creature"){
            Creature* cret = create_creature_role(elem, being_data);
            creature_roles.insert(std::make_pair(cret->getRole(), cret));
            beings.push_back(cret);
        }
        else if (being_data.Type == "Eldritch Horror"){
            EldritchHorror* eldr = create_eldritch_role(elem, being_data);
            creature_roles.insert(std::make_pair(eldr->getRole(), eldr));
            beings.push_back(eldr);
        }
        else throw std::runtime_error("Unrecognized type declared in roles loading file");
    }
    in.close();
}

void Roles::printTypes() {
    std::cout << " 1: Investigator" << std::endl;
    std::cout << " 2: Person" << std::endl;
    std::cout << " 3: Creature" << std::endl;
    std::cout << " 4: Eldritch Horror" << std::endl;
}

void Roles::printTypeRoles(const std::string& type) {
    int i=1;
    if (type == "Person" || type == "Investigator"){
        for (auto person: person_roles){
            std::cout << " " << i << ": " << person.second->getRole() << std::endl;
            i++;
        }
    }
    else if (type == "Creature"){
        for (auto creature: creature_roles){
            std::cout << " " << i << ": " << creature.second->getRole() << std::endl;
            i++;
        }
    }
    else if (type == "Eldritch Horror"){
        for (auto eldritch: eldritch_roles){
            std::cout << " " << i << ": " << eldritch.second->getRole() << std::endl;
            i++;
        }
    }
    else {
        throw std::runtime_error("Type not defined");
    }

    if (i == 1){
        std::cout << "This type has no roles yet!" << std::endl;
    }
}

std::string Roles::getRole(const string &type, int index) {
    index--;
    int curr_idx=0;
    if (type == "Person" || type == "Investigator"){
        for (auto person: person_roles){
            if (index == curr_idx){
                return person.second->getRole();
            }
            curr_idx++;
        }
    }
    else if (type == "Creature"){
        for (auto creature: creature_roles){
            if (index == curr_idx){
                return creature.second->getRole();
            }
            curr_idx++;
        }
    }
    else if (type == "Eldritch Horror"){
        for (auto eldritch: eldritch_roles){
            if (index == curr_idx){
                return eldritch.second->getRole();
            }
            curr_idx++;
        }
    }
    else {
        throw std::runtime_error("Type not defined");
    }
    throw std::runtime_error("Type not defined");
}
























