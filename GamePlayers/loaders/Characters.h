#ifndef GAMEPLAYERS_CHARACTERS_H
#define GAMEPLAYERS_CHARACTERS_H
#include "../categories/Person.h"
#include "../categories/Creature.h"
#include "../categories/EldritchHorror.h"
#include "../character_builder_models/beingCharacter.h"
#include "../character_builder_models/personCharacter.h"
#include "../character_builder_models/creatureCharacter.h"
#include "../character_builder_models/eldritchCharacter.h"
#include "../json.hpp"
#include <unordered_map>
#include "Roles.h"

class Characters {
public:
    // If user wants to load in a roster
    explicit Characters(Roles& roles, const std::string& filename);
    // Instantiate without loading any characters
    explicit Characters(Roles& roles);
    // Add a person character
    void add(Person* person);
    // Add a creature character
    void add(Creature* creature);
    // Add an eldritch character
    void add(EldritchHorror* eldritch);
    // Save current roster to a file
    void save(std::string filename);
    // Remove a character. Make sure to check by using the Role class if the given role exists for a type before removal
    void removeCharacter(const std::string& type, const std::string& roleName, const std::string& name);

    void removeAllCharactersOfRole(const std::string& type, const std::string& roleName);

    // Print all the characters in the roster
    void printAllCharDetails();
    // Print a numbered list of all characters in a given role (No details just names)
    void printAllCharOfType(const std::string &typeName);
    // Reset to defaults
    void reset();
    // load a roster
    void load(std::string filename);

    bool containsAny(const std::string& type);

    EldritchHorror* getEldritchCharacter(int index);
    Creature* getCreatureCharacter(int index);
    Person* getPersonCharacter(int index);
    Person* getInvestigatorCharacter(int index);
    int getMaxIndex(const std::string& type);


protected:
    static Person* create_person_character(json& j, char_being::loader& being_data);
    static Creature* create_creature_character(json&j, char_being::loader&being_data);
    static EldritchHorror* create_eldritch_character(json&j, char_being::loader&being_data);
    void addToRoleCount(const std::string& role);
    std::string getNPCname(const std::string& role);
    bool role_exists(const std::string& roleName);

private:
    Roles& roles;
    std::unordered_map<std::string, int> role_counter;
    std::vector<Being*> beings;
    std::vector<Person*> investigator_characters{};
    std::vector<Person*> person_characters{};
    std::vector<Creature*> creature_characters{};
    std::vector<EldritchHorror*> eldritch_characters{};
};


#endif //GAMEPLAYERS_CHARACTERS_H
