#ifndef GAMEPLAYERS_ROLES_H
#define GAMEPLAYERS_ROLES_H
#include <map>
#include <vector>
#include "../role_builder_models/beingRole.h"
#include "../role_builder_models/personRole.h"
#include "../role_builder_models/creatureRole.h"
#include "../role_builder_models/eldritchRole.h"
#include "../categories/Person.h"
#include "../categories/Creature.h"
#include "../categories/EldritchHorror.h"
#include "../json.hpp"
/*
 * The Roles class only loads in roles. All modifications to the roles on runtime are not stored
 * in the roles. The roles themselves can only be added through the json file.
 *
 * When a character is created with any of the available methods, the class is returned. Once the user is ready
 * to save the character, it should be passed into the Characters class where the class will then be
 * serialized into json format and saved to a different file.
 *
 */
class Roles {
public:
    explicit Roles(const std::string& filename);
    Roles();
    // Allow user to create a new person role
    void createPersonRole(role_being::loader& being_data, role_person::loader& person_data);
    // Allow user to create a creature role
    void createCreatureRole(role_being::loader& being_data, role_creature::loader& creature_data);
    // Allow user to create a new eldritch
    void createEldritchRole(role_being::loader& being_data, role_creature::loader& creature_data, role_eldritch::loader& eldritch_data);
    // Create a person
    Person* createPerson(const std::string& roleName, const std::string& name, const std::string& gender);
    // Create a creature
    Creature* createCreature(const std::string& roleName);
    // Create an eldritch horror
    EldritchHorror* createEldritch(const std::string& roleName);
    // Print all available roles
    void printRoles();
    // Print available roles for a given type
    void printTypeRoles(const std::string& type);
    // Print a specific role's details
    void printRoleDetails(const std::string& roleName);
    // Print the available types numbered from 1 to x
    void printTypes();
    // Print all roles and all their attributes
    void printAllRoleDetails();
    // Remove role from current roles
    void removeRole(const std::string& type, const std::string& roleName);
    // Save all current roles to a file
    void save(std::string filename);
    // Check if a role exists for some type
    bool roleExists(const std::string& type, const std::string& roleName);
    // Get role settings to load into a character
    role_being::loader *getRoleSettings(const string& type, const string& roleName);
    // Reset to empty default
    void reset();
    void load(std::string filename);

    std::string getRole(const std::string& type, int index);

protected:
    static Person* create_person_role(json& j, role_being::loader& being_data);
    static Creature* create_creature_role(json&j, role_being::loader&being_data);
    static EldritchHorror* create_eldritch_role(json&j, role_being::loader&being_data);

private:
    std::vector<Being*> beings{};
    std::map<std::string, Person*> person_roles{};
    std::map<std::string, Creature*> creature_roles{};
    std::map<std::string, EldritchHorror*> eldritch_roles{};
};


#endif //GAMEPLAYERS_ROLES_H
