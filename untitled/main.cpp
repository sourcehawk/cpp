#include <iostream>

int *f(int *t, int *u) {
    if (*t == 0) {
        *t = 50;
        return u;
    }
    else {
        return t;
    }
}
void g(int x, int y) {
    f(&x, &y);
    f(&x, &y);
    std::cout << "y: " << y << std::endl;
}
void h(int x, int y) {
    int *p;
    p = &x;
    p = f(p, &y);
    f(p, &y);
    std::cout << "y: " << y << std::endl;
}


int main() {
    std::cout << "(i) g(1, 2)" << std::endl;
    g(1, 2);
    std::cout << "(i) h(1, 2)" << std::endl;
    h(1, 2);
    std::cout << "(ii) g(1, 0)" << std::endl;
    g(1, 0);
    std::cout << "(ii) h(1, 0)" << std::endl;
    h(1, 0);
    std::cout << "(iii) g(0, 0)" << std::endl;
    g(0, 0);
    std::cout << "(iii) h(0, 0)" << std::endl;
    h(0, 0);

    return 0;
}
