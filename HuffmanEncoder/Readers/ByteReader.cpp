//
// Created by agirm on 23/02/2021.
//
#include <fstream>
#include <vector>
#include <bits/stdc++.h>
#include "ByteReader.h"
#include <utility>
#define BUFFER_SIZE 100000 // 100k bytes ~ 100kiB

// Take two pairs
template <typename T1, typename T2>

/* Comparator implementation for sorting two pairs. Compares pairs by value first and key if necessary */
struct sortByValue {
    bool operator()(const std::pair<T1, T2>& a, const std::pair<T1, T2>& b) const{
        if (a.second != b.second) {
            return a.second > b.second;
        }
        return a.first > b.first;
    }
};

ByteReader::ByteReader() {
    this->filename = nullptr;
    this->fileRemainingBytes = 0;
    this->latestBufferSize = 0;
    this->fileReadBytes = 0;
    this->fileByteSize = 0;
    this->initByte = false;
}


void ByteReader::initByteReader(const char* infile) {
    this->setInitial();
    this->initByte = true;
    this->filename = infile;
    this->initialRead(this->filename);
}

std::vector<std::pair<char, int> > ByteReader::getByteFrequencyMap() {
    if (!this->initByte){
        throw std::runtime_error("ByteReader not initialized!");
    }

    if (this->eof()){
        throw std::runtime_error("EOF in file when fetching byte frequency map.");
    }
    int count[257]={0};
    while(!this->eof()){
        char* stream = this->getArrayBufferStream();
        // Counting byte occurrence into array where i is the byte
        for (int i = 0; i<this->latestBufferSize; i++){
            count[(unsigned char) stream[i]]++;
        }
        delete[] stream;
    }
    std::vector<std::pair<char, int> > freq_vector;
    // Creating vector pairs with count as key and value as char/byte
    for (int i=0; i<257; i++){
        if (count[i]!=0){
            auto c = (unsigned char) i;
            freq_vector.emplace_back(c, count[i]);
        }
    }
    // Sort vector from smallest to largest frequency
    std::sort(freq_vector.begin(), freq_vector.end(), sortByValue<char, int>());
    this->resetReadCount();

    /* Pringing frequencies
    for (auto const& pair: freq_vector){
        if (pair.first == '\n'){
            std::cout << "\\n" << ": " << "Frequency: " << pair.second << std::endl;
        }
        else {
            std::cout << pair.first << ": " << "Frequency: " << pair.second << std::endl;
        }
    }
    std::cout << std::endl;*/


    return freq_vector;

}

int ByteReader::getFileSize() const {
    if (!this->initByte){
        throw std::runtime_error("ByteReader not initialized!");
    }
    return this->fileByteSize;
}

bool ByteReader::eof() const {
    if (!this->initByte){
        throw std::runtime_error("ByteReader not initialized!");
    }
    return this->fileByteSize <= this->fileReadBytes;
}

int ByteReader::getLatestBufferSize() const {
    if (!this->initByte){
        throw std::runtime_error("ByteReader not initialized!");
    }
    return this->latestBufferSize;
}

char *ByteReader::getArrayBufferStream() {
    if (!this->initByte){
        throw std::runtime_error("ByteReader not initialized!");
    }
    if (this->fileRemainingBytes < BUFFER_SIZE){
        char* buffer = new char[fileRemainingBytes];
        // Open file in binary mode
        std::ifstream f_in(this->filename, std::ios::binary);
        // Go to where we left off
        f_in.seekg(this->fileReadBytes, std::ios::beg);
        // Read rest of file
        f_in.read(&buffer[0], this->fileRemainingBytes);

        this->latestBufferSize = fileRemainingBytes;
        this->fileRemainingBytes = 0;
        this->fileReadBytes = this->fileByteSize;
        return buffer;
    }
    else {
        char* buffer = new char[BUFFER_SIZE];

        // Open file in binary mode
        std::ifstream f_in(this->filename, std::ios::binary);
        // Go to where we left off
        f_in.seekg(this->fileReadBytes, std::ios::beg);
        // Read up to buffer
        f_in.read(buffer, BUFFER_SIZE);

        this->latestBufferSize = BUFFER_SIZE;
        this->fileReadBytes+=BUFFER_SIZE;
        this->fileRemainingBytes-=BUFFER_SIZE;
        return buffer;
    }
}

std::vector<char> ByteReader::getVectorBufferStream() {
    if (!this->initByte){
        throw std::runtime_error("ByteReader not initialized!");
    }
    if (this->fileRemainingBytes < BUFFER_SIZE){
        std::vector<char> buffer(fileRemainingBytes);
        // Open file in binary mode
        std::ifstream f_in(this->filename, std::ios::binary);
        // Go to where we left off
        f_in.seekg(this->fileReadBytes, std::ios::beg);
        // Read rest of file
        f_in.read(&buffer[0], this->fileRemainingBytes);
        f_in.close();

        this->latestBufferSize = this->fileRemainingBytes;
        this->fileRemainingBytes = 0;
        this->fileReadBytes = this->fileByteSize;
        return buffer;
    }
    else {
        std::vector<char> buffer(BUFFER_SIZE);

        // Open file in binary mode
        std::ifstream f_in(this->filename, std::ios::binary);
        // Go to where we left off
        f_in.seekg(this->fileReadBytes, std::ios::beg);
        // Read rest of file
        f_in.read(&buffer[0], BUFFER_SIZE);
        f_in.close();

        this->latestBufferSize = BUFFER_SIZE;
        this->fileReadBytes+=BUFFER_SIZE;
        this->fileRemainingBytes-=BUFFER_SIZE;
        return buffer;
    }
}

void ByteReader::resetReadCount() {
    this->fileReadBytes = 0;
    this->fileRemainingBytes = this->fileByteSize;
}

std::vector<char> ByteReader::getVectorBufferStream(int byteAmount) {
    if (!this->initByte){
        throw std::runtime_error("ByteReader not initialized!");
    }
    if (this->fileRemainingBytes<byteAmount){
        throw std::runtime_error("Requesting more bytes than available!");
    }
    else {
        std::vector<char> buffer(byteAmount);

        // Open file in binary mode
        std::ifstream f_in(this->filename, std::ios::binary);
        // Go to where we left off
        f_in.seekg(this->fileReadBytes, std::ios::beg);
        // Read rest of file
        f_in.read(&buffer[0], byteAmount);
        f_in.close();

        this->latestBufferSize = byteAmount;
        this->fileReadBytes+=byteAmount;
        this->fileRemainingBytes-=byteAmount;
        return buffer;
    }
}

const char *ByteReader::getArrayBufferStream(int byteAmount) {
    if (this->fileRemainingBytes<byteAmount){
        throw std::runtime_error("Requesting more bytes than available!");
    }
    if (!this->initByte){
        throw std::runtime_error("No ByteReader initialization");
    }
    else {
        char* buffer = new char[byteAmount];

        // Open file in binary mode
        std::ifstream f_in(this->filename, std::ios::binary);
        // Go to where we left off
        f_in.seekg(this->fileReadBytes, std::ios::beg);
        // Read rest of file
        f_in.read(&buffer[0], byteAmount);

        this->latestBufferSize = byteAmount;
        this->fileReadBytes+=byteAmount;
        this->fileRemainingBytes-=byteAmount;
        return buffer;
    }
}

// No longer needed
char ByteReader::lastByte() {
    if (!this->initByte){
        throw std::runtime_error("ByteReader not initialized!");
    }
    if (this->eof()){
        throw std::runtime_error("EOF in file. (lastByte)");
    }

    std::ifstream f_in(this->filename, std::ios::binary);
    // Go to where we left off
    f_in.seekg(this->fileByteSize-1, std::ios::beg);
    char lastByte[1]={0};
    f_in.read(lastByte, 1);
    f_in.close();
    return lastByte[0];
}

int ByteReader::remainingBytes() const {
    return this->fileRemainingBytes;
}

void ByteReader::reduceReadCount(int howMany) {
    if (this->fileRemainingBytes == 0 || this->fileRemainingBytes-howMany<0){
        throw std::runtime_error("Not enough bytes remaining to reduce!");
    }
    this->fileReadBytes += howMany;
    this->fileRemainingBytes -=howMany;
}
