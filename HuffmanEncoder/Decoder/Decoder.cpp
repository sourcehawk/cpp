//
// Created by agirm on 24/02/2021.
//

#include "Decoder.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>

void decompressFile(Decoder& dc, const std::string& output_filename){
    std::ofstream out;
    out.open(output_filename, std::ios::binary | std::ios::out);

    while(!dc.byteReader->eof()){
        std::vector<char> stream = dc.byteReader->getVectorBufferStream();

        /* In the edge case that there is only one byte remaining in the file to decode
         * (which is actually the byte that says how many bits to read in the second last byte.
         * We need to send that byte with the stream
         * */
        if (dc.byteReader->remainingBytes() == 1){
            char last_byte = dc.byteReader->lastByte();
            dc.byteReader->reduceReadCount(1);
            stream.emplace_back(last_byte);
        }
        /* If we're at last iteration we need to tell the decoder to read the info in the last
        *  byte regarding how many bytes in the previous to read
        */
        if (dc.byteReader->eof()){
            std::vector<char> fileContent = dc.huffmanTree->decode(stream, true);
            out.write((char*)&fileContent[0], fileContent.size());
        }
        /*
         * If we're not at last iteration we simply pass the stream for decoding.
         */
        else {
            std::vector<char> fileContent = dc.huffmanTree->decode(stream, false);
            out.write((char*)&fileContent[0], fileContent.size());
        }
    }
    out.close();
}

void Decoder::decode(const std::string &input_filename, const std::string &output_filename) {
    this->byteReader = new ByteReader();
    this->byteReader->initByteReader(input_filename.c_str());
    /*
     * The size of the header in bits is stored in the first two bytes of the encoded file.
     * We need to extract those two bytes and turn it into a number in order
     * to know how many bytes we need to fetch to get the header.
     * */
    // Get first two bytes in the encoded file
    std::vector<char> header_size_bytes = this->byteReader->getVectorBufferStream(2);

    // Convert the two bytes to a single number
    unsigned char firstByte = header_size_bytes[0];
    unsigned char secondByte = header_size_bytes[1];
    int bitCount = 0;
    bitCount = bitCount | (int)((unsigned int) firstByte)<<8;
    bitCount = bitCount | (int)((unsigned int) secondByte);
    int totalBits = (int) bitCount;

    // Get the byte size of the header in order to extract it from the encoded file
    int totalBytes = (int) totalBits/8;
    if (totalBits%8 != 0){
        totalBytes++;
    }

    // Get header given the bytes we need to read
    std::vector<char> header = this->byteReader->getVectorBufferStream(totalBytes);

    // Create huffman tree from header
    this->huffmanTree = new HuffmanTree(std::move(header), totalBits);
    decompressFile(*this, output_filename);
}