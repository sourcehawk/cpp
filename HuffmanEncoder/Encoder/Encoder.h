//
// Created by agirm on 23/02/2021.
//

#ifndef HUFFMANENCODER_ENCODER_H
#define HUFFMANENCODER_ENCODER_H
#include <vector>
#include "../HuffmanTree/HuffmanTree.h"
#include "../Readers/ByteReader.h"

class Encoder {
    friend void compressFile(Encoder& ec, const std::string& output_filename);
public:
    void encode(const std::string&  input_filename, const std::string& output_filename);
    ByteReader* byteReader{};
    HuffmanTree* huffmanTree{};
};


#endif //HUFFMANENCODER_ENCODER_H
