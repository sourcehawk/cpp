#include <iostream>
#include <vector>
#include <cstring>
#include "Decoder/Decoder.h"
#include "Encoder/Encoder.h"
#include <chrono>


int main(int argc, char *argv[]){
    std::cout << argc << std::endl;

    /* Im not sure whether all systems initialize the first argument as call path.
     * If you run into problems try changing the argc to 3 and the indexes of the input arguments. */

    if (argc!=4){
        std::cout << "Invalid number of arguments passed. [-c/-u] [infile] [outfile]." << std::endl;
        return 0;
    }
    const char *method = argv[1];
    std::string filename = argv[2];
    std::string outfile = argv[3];

    /*
     * COMPRESSING FILE
     */
    if (std::strcmp(method, "-c")==0){
        Encoder enc;
        try {
            std::cout << "Compressing file: " << filename << std::endl;
            std::cout << "Output file: " << outfile << std::endl;

            auto start = std::chrono::high_resolution_clock::now();
            enc.encode(filename, outfile);
            auto finish = std::chrono::high_resolution_clock::now();
            std::cout << "Encoding time: " << std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count() << "ms\n";
        }
        catch ( const std::exception & ex){
            std::cout << ex.what() << std::endl;
        }
    }

    /*
     * DECOMPRESSING FILE
     */
    else if (std::strcmp(method, "-u")==0){
        Decoder dec;
        try {
            std::cout << "Decompression file: " << filename << std::endl;
            std::cout << "Output file: " << outfile << std::endl;

            auto start = std::chrono::high_resolution_clock::now();
            dec.decode(filename, outfile);
            auto finish = std::chrono::high_resolution_clock::now();
            std::cout << "Decoding time: " << std::chrono::duration_cast<std::chrono::milliseconds>(finish-start).count() << "ms\n";
        }
        catch( const std::exception & ex ) {
            std::cout << ex.what() << std::endl;
        }
    }

    /*
     * RETURN ERROR
     */
    else {
        std::cout << "Command not recognized. To decompress use [-u], to compress use [-c]" << std::endl;
    }
    return 0;
}