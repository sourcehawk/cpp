# VERSION B
1. Any filesize supported
2. Any amount of bytes supported
3. Compressing a file with 128 different characters has a compression ratio of roughly 70%.
4. Optimal compression for arbitrarily random text files. Not for distributed randomness.
   (60-70% of original size for actual written files/documents such as a books and novels)
## Running the program
`[-c/-u] [infile] [outfile]`
* -c = Compress
* -u = Decompress

* Linux: `./linux_compressor`
* Windows: `windows_compressor`
* Mac: Compile it yourself with the compile command below just change the `run.exe`git  to something else


Im not sure whether all systems initialize the first argument as call path.
If you run into problems try changing the argc to 3 and the indexes of the input arguments in main.cpp
```cpp
int main(int argc, char *argv[]){
    std::cout << argc << std::endl;

    if (argc!=4){ // <-- Change to 3
        std::cout << "Invalid number of arguments passed. [-c/-u] [infile] [outfile]." << std::endl;
        return 0;
    }
    const char *method = argv[1];   //<-- Change to 0
    std::string filename = argv[2]; //<-- Change to 1
    std::string outfile = argv[3];  //<-- Change to 2
```

## Compile command
`g++ -o run.exe main.cpp Decoder/Decoder.cpp Encoder/Encoder.cpp HuffmanTree/HuffmanTree.cpp Readers/ByteReader.cpp BinaryArray/BinaryArray.cpp  -std=c++11`


## How it works
The basics of how our compression works

#### Compression
1. Initialize a counter int array of size 256. Now each index in this array can represent one of the 255 bytes possible.
   
2. Read the file buffered (in chunks) into an char array, iterate over each element in the buffer and count the byte 
   occurrence until file is empty.\
   `count[buffer[i]] += 1`
   
3. Generate a frequency map from the final count, but only include characters that didn't have a count of 0.
    ```
    map[byte] = frequency
    Result:
    a: Frequency: 2
    b: Frequency: 4
    c: Frequency: 4
    d: Frequency: 8
    e: Frequency: 16
    ```
4. Construct a left leaning huffman tree from the frequency map.
   
5. Generate header using the huffman tree. 
   Recursively iterate the tree in post order and mark all non leaves as a single bit 1. All leaves we mark with a
   single bit 0. Right after a leaf we add that leave's corresponding byte/letter to the header.
   The image below depicts how this is done. Note that the tree will also build subtrees on the right if appropriate but
   this is a simplified example.\
   ![image](Images/tree.PNG)\
   Before inserting the header we add two new bytes in front of the generated header that signify the number of _bits_
   to read after the first two bytes. This is to be able to know when the header is done on decompressing.
   
5. Generate bitcode vector/map from the huffman tree.
```
    bitcodeMap[byte] = pair{bitcode, size of bitcode}
    Result:
    e: Bitcode: 00000000, Size of bitcode: 1
    d: Bitcode: 00000010, Size of bitcode: 2
    c: Bitcode: 00000110, Size of bitcode: 3
    a: Bitcode: 00001111, Size of bitcode: 4
    b: Bitcode: 00001110, Size of bitcode: 4
```
6. After inserting the header, we now read the entire file once again and map each character with the bitcode map to
get the representation of each byte. Once our write buffer is full or the file end reached we write a chunk to the file.
Before we insert the last chunk into the file, we add one last byte at the end that signifies how many bits should be
decoded in the next last byte. **All bitcodes are shifted into bytes so not a single bit is wasted.**
   
#### Decompression
1. Recursively generate a huffman tree from the header. Since we stored the header as a tree we can now reconstruct it
without ever knowing the frequency of each character.
   
2. Read the encoded file buffered into a char vector.

3. Decode the buffer and write to file. When we created the bitcodes, each single bit 1 signified going left in the tree, 
   and each single bit 0 signified going to the right in the tree. We can now recursively go through the tree with this 
   information. Once we are at a leaf node, we return the character it holds.
   
4. Repeat until encoded file has been read entirely. Once we are reading the last buffer, we check the last byte for
how many characters to decode of the second last byte.
   
## Maximum file size
As mentioned before the program should be able to compress anything, since it uses a buffer to fetch
a defined number of bytes at a time. However, anything not alphabetically related (more than 128 unique letters)
is just not worth compressing at all.

## Compression time
Can possibly be improved by increasing the buffer size in the encoder and byte reader.
It is currently set at 100k bytes.

Also the compression and decompression times are significantly faster (30-40% faster) on my linux subsystem compared to windows. 
No idea why but cool.
```
(Windows compression times)
Ryzen 5 3600x 3.8-4.4GHz

Compressing file: elise_both.wav
Output file: out.huff
Encoding time: 1837ms

Decompression file: out.huff
Output file: elise_out.wav
Decoding time: 2352ms


Compressing file: forest.wav
Output file: out.huff
Encoding time: 3708ms

Decompression file: out.huff
Output file: forest_out.wav
Decoding time: 2792ms
```
I also created a **250MB** file using this [wordfile](https://github.com/dwyl/english-words/blob/master/words.txt), 
i.e by copy pasting it over and over again and compressed and decompressed it.

```
Compressing file: words.txt
Output file: out.huff
Encoding time: 71083ms ~ 71 seconds

Decompression file: out.huff
Output file: words.txt
Decoding time: 22354ms ~ 22 seconds
```

Basically you can compress any size file you want. However the compression ratio
varies greatly on the number of bytes that need representation and the occurrence
rates of each byte.

The 250MB file I compressed has over 128 unique characters and the compressed file
was 180MB. Giving it a compression ratio of:
`CR = 180/250 = 0.7`
Which honestly is not that great. Also if many bytes have roughly the same occurrence rate,
letters that occur frequently will have large bit codes which makes the compression horrendous.


**Note**\
I added the file the_hunger_games.txt for you to test the text file. (It's a book)
If you want to test something way larger just copy paste it over and over until it's 
large enough for your needs!
