#include "DeliveryAPI/DeliveryAPI.h"
#include <functional>
#include <thread>
#include <chrono>
using namespace std::placeholders;

int main() {
    DeliveryAPI delivery;
    auto function_pointer = std::bind(&DeliveryAPI::server_response_handler, &delivery, _1); //_1 means take one argument
    auto* client = new Client(PIZZA_DELIVERER, function_pointer);
    client->start();
    std::this_thread::sleep_for (std::chrono::seconds(1));
    delivery.add_client(client);
    delivery.start();
    return 0;
}


/*
g++ -o delivery delivery.cpp FileReader/FileReader.cpp FileWriter/FileWriter.cpp PizzaPrinter/PizzaPrinter.cpp PizzaDeliverer/PizzaDeliverer.cpp Networking/TCP/TCP.cpp Networking/Request/Request.cpp Networking/Response/Response.cpp Networking/Parser/Parser.cpp Networking/Client/Client.cpp DeliveryAPI/DeliveryAPI.cpp -pthread -std=c++11


g++ -o delivery delivery.cpp
FileReader/FileReader.cpp
FileWriter/FileWriter.cpp
PizzaPrinter/PizzaPrinter.cpp
PizzaDeliverer/PizzaDeliverer.cpp
Networking/TCP/TCP.cpp
Networking/Request/Request.cpp
Networking/Response/Response.cpp
Networking/Parser/Parser.cpp
Networking/Client/Client.cpp
DeliveryAPI/DeliveryAPI.cpp -pthread -std=c++11
 */