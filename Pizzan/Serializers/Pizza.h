#ifndef PIZZAN_PIZZA_H
#define PIZZAN_PIZZA_H
#include <vector>
#include <string>
#include "./PizzaTopping.h"
#include "../json.hpp"

class Pizza {
public:
    int id     = -1;
    int size   =  0;
    int bottom =  0;
    int price  =  0;
    std::string name = "No-name";
    std::vector<PizzaTopping*> toppings={};

    static void to_json(const Pizza& pizza, json &j_pizza)
    {
        json j_toppings = json::array();
        for (auto top: pizza.toppings)
        {
            json j_topping = *top;
            j_toppings.push_back(j_topping);
        }
        j_pizza = json{{"size", pizza.size},
                    {"id", pizza.id},
                    {"name", pizza.name},
                    {"bottom", pizza.bottom},
                    {"price", pizza.price},
                    {"toppings", j_toppings}};
    }
    static Pizza* from_json(const json& j)
    {
        Pizza* pizza = new Pizza();
        std::vector<PizzaTopping*> pizza_toppings = {};
        for (auto& j_topping: j.at("toppings"))
        {
            PizzaTopping p_topping = j_topping.get<PizzaTopping>();
            pizza_toppings.push_back(new PizzaTopping(p_topping.id, p_topping.type, p_topping.amount));
        }
        pizza->toppings = pizza_toppings;
        j.at("id").get_to(pizza->id);
        j.at("size").get_to(pizza->size);
        j.at("name").get_to(pizza->name);
        j.at("price").get_to(pizza->price);
        j.at("bottom").get_to(pizza->bottom);

        return pizza;
    }
};




#endif //PIZZAN_PIZZA_H
