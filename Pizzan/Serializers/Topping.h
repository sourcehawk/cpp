#ifndef PIZZAN_TOPPING_H
#define PIZZAN_TOPPING_H
#include <string>
#include "../json.hpp"
using json = nlohmann::json;

namespace topping_serializer {
    struct Topping {
        int id;
        int type;
        std::string name;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Topping, id, type, name)
}

using namespace topping_serializer;

#endif //PIZZAN_TOPPING_H
