#ifndef PIZZAN_PIZZATOPPING_H
#define PIZZAN_PIZZATOPPING_H

#include <string>
#include "../json.hpp"
using json = nlohmann::json;

namespace pizza_topping_serializer {
    struct PizzaTopping {
        PizzaTopping() = default;
        PizzaTopping(int id, int type, int amount): id(id), type(type), amount(amount) {}
        PizzaTopping(int id, int type): id(id), type(type) {}
        int id;
        int type;
        int amount = 1;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(PizzaTopping, id, type, amount)
}

using namespace pizza_topping_serializer;

#endif //PIZZAN_PIZZATOPPING_H
