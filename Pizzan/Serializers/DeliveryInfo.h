#ifndef PIZZAN_DELIVERYINFO_H
#define PIZZAN_DELIVERYINFO_H

#include <string>
#include "../json.hpp"
using json = nlohmann::json;

namespace delivery_info_serializer {
    struct DeliveryInfo {
        DeliveryInfo() = default;
        DeliveryInfo(const std::string &name, const std::string &address, const std::string &city, const std::string &postal_code, const std::string &phone):
            name(name), address(address), city(city), postal_code(postal_code), phone(phone){};
        std::string name;
        std::string address;
        std::string city;
        std::string postal_code;
        std::string phone;
        DeliveryInfo* getHeapCopy()
        {
            return new DeliveryInfo(name, address, city, postal_code, phone);
        }
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DeliveryInfo, name, address, city, postal_code, phone);
}

using namespace delivery_info_serializer;

#endif //PIZZAN_DELIVERYINFO_H
