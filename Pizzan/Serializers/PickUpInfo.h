#ifndef PIZZAN_PICKUPINFO_H
#define PIZZAN_PICKUPINFO_H

#include <string>
#include "../json.hpp"
using json = nlohmann::json;

namespace pickup_info_serializer {
    struct PickUpInfo {
        PickUpInfo() = default;
        PickUpInfo(const std::string &name,const std::string &phone):
                name(name), phone(phone){};
        std::string name;
        std::string phone;

        PickUpInfo* getHeapCopy()
        {
            return new PickUpInfo(name, phone);
        }
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(PickUpInfo, name, phone)
}

using namespace pickup_info_serializer;

#endif //PIZZAN_PICKUPINFO_H
