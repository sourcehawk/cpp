#ifndef PIZZAN_TOPPINGTYPE_H
#define PIZZAN_TOPPINGTYPE_H
#include <string>
#include "../json.hpp"
using json = nlohmann::json;

namespace topping_type_serializer {
    struct ToppingType {
        int id;
        int price;
        std::string name;
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ToppingType, id, price, name)
}

using namespace topping_type_serializer;

#endif //PIZZAN_TOPPINGTYPE_H
