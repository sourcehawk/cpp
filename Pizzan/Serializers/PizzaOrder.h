#ifndef PIZZAN_PIZZAORDER_H
#define PIZZAN_PIZZAORDER_H
#include "Pizza.h"
#include "DeliveryInfo.h"
#include "PickUpInfo.h"
#include <iostream>
#include <vector>
#include "../json.hpp"
using nlohmann::json;

class PizzaOrder {
public:
    PizzaOrder() = default;
    PizzaOrder(std::vector<Pizza*> pizzas, PickUpInfo* pickUpInfo, int totalPrice):
        pizzas(pizzas), pickUpInfo(pickUpInfo), totalPrice(totalPrice) {}
    PizzaOrder(std::vector<Pizza*> pizzas, DeliveryInfo* deliveryInfo, int totalPrice):
        pizzas(pizzas), deliveryInfo(deliveryInfo), totalPrice(totalPrice){ delivery = true; }

    std::string orderID = "none";
    bool delivery = false;
    bool complete = false;
    std::vector<Pizza*> pizzas = {};
    DeliveryInfo* deliveryInfo = nullptr;
    PickUpInfo* pickUpInfo = nullptr;
    int totalPrice;

    static void to_json(const PizzaOrder& pizzaOrder, json &j_order)
    {
        json j_pizzas = json::array();
        for (auto pizza: pizzaOrder.pizzas){
            json j_pizza;
            pizza->to_json(*pizza, j_pizza);
            j_pizzas.push_back(j_pizza);
        }
        json j_delivery_info = "none";
        if (pizzaOrder.delivery)
            j_delivery_info = *pizzaOrder.deliveryInfo;

        json j_pickup_info = "none";
        if (!pizzaOrder.delivery)
            j_pickup_info = *pizzaOrder.pickUpInfo;

        json json_pizza_order;
        j_order["order"] = {{"delivery", pizzaOrder.delivery},
                             {"orderID", pizzaOrder.orderID},
                             {"complete", pizzaOrder.complete},
                             {"totalPrice", pizzaOrder.totalPrice},
                             {"pizzas", j_pizzas},
                             {"pickUpInfo", j_pickup_info},
                             {"deliveryInfo", j_delivery_info}};
    }

    static PizzaOrder* from_json(const json& j)
    {
        PizzaOrder* some_order = new PizzaOrder();
        json j_pizzas = json::array();
        j_pizzas = j["order"]["pizzas"];

        for (const auto& pizza: j_pizzas)
        {
            Pizza* some_pizza = Pizza::from_json(pizza);
            some_order->pizzas.push_back(some_pizza);
        }
        some_order->orderID = j["order"]["orderID"].get<std::string>();
        some_order->totalPrice = j["order"]["totalPrice"].get<int>();
        some_order->complete = j["order"]["complete"].get<bool>();
        some_order->delivery = j["order"]["delivery"].get<bool>();
        if (some_order->delivery)
        {
            DeliveryInfo del_info = j["order"]["deliveryInfo"].get<DeliveryInfo>();
            DeliveryInfo* deliveryInfo = del_info.getHeapCopy();
            some_order->deliveryInfo = deliveryInfo;
        }
        else
        {
            PickUpInfo pu_info = j["order"]["pickUpInfo"].get<PickUpInfo>();
            PickUpInfo* pickupInfo = pu_info.getHeapCopy();
            some_order->pickUpInfo = pickupInfo;
        }
        return some_order;
    }
};

#endif //PIZZAN_PIZZAORDER_H
