#include "CustomerAPI/CustomerAPI.h"
#include <functional>
#include <thread>
#include <chrono>
using namespace std::placeholders;
int main(){
    CustomerAPI customer;
    auto function_pointer = std::bind(&CustomerAPI::server_response_handler, &customer, _1); //_1 means take one argument
    auto* client = new Client(PIZZA_MAKER, function_pointer);
    client->start();
    std::this_thread::sleep_for (std::chrono::seconds(1));
    customer.add_client(client);
    customer.start();
    return 0;
}

/*
COMPILE COMMAND:
g++ -o customer customer.cpp FileReader/FileReader.cpp PizzaPrinter/PizzaPrinter.cpp PizzaPicker/PizzaPicker.cpp PizzaMaker/PizzaMaker.cpp Networking/TCP/TCP.cpp Networking/Request/Request.cpp Networking/Response/Response.cpp Networking/Parser/Parser.cpp Networking/Client/Client.cpp CustomerAPI/CustomerAPI.cpp -pthread -std=c++11

 g++ -o customer
 customer.cpp
 FileReader/FileReader.cpp
 PizzaPrinter/PizzaPrinter.cpp
 PizzaPicker/PizzaPicker.cpp
 PizzaMaker/PizzaMaker.cpp
 Networking/TCP/TCP.cpp
 Networking/Request/Request.cpp
 Networking/Response/Response.cpp
 Networking/Parser/Parser.cpp
 Networking/Client/Client.cpp
 CustomerAPI/CustomerAPI.cpp -pthread -std=c++11

 */