#include "BakeryAPI.h"
#include "../Networking/Parser/Parser.h"
#include <iomanip>
#include <cstdlib>
#include "../json.hpp"
using nlohmann::json;
#define ID_W 10
#define OID_W 15
#define C_W 30
#define T_W 25
#define S_W 20



void BakeryAPI::print_orders() {
    std::lock_guard<std::mutex> guard(terminal_mutex);
    std::system("clear");
    print_command_header();
    print_list_header();
    if (pizzaBaker.orders.empty())
    {
        std::string msg = "NO ORDERS";
        std::cout << msg << std::endl;
        return;
    }
    for (auto& order: pizzaBaker.orders)
    {
        BakerOrder& b_order = *order.second;
        if (b_order.order->delivery)
            format_list_element(std::to_string(order.first),
                                b_order.order->orderID,
                                b_order.order->deliveryInfo->name,
                                b_order.time_string,
                                b_order.status_string);
        else {
            format_list_element(std::to_string(order.first),
                                b_order.order->orderID,
                                b_order.order->pickUpInfo->name,
                                b_order.time_string,
                                b_order.status_string);
        }
    }
}

void BakeryAPI::print_list_header() {
    std::string id, oid, cm, tm, st;
    id = "ID";
    oid = "ORDER ID";
    cm = "CUSTOMER";
    tm = "ORDER TIME";
    st = "ORDER STATUS";
    format_list_element(id, oid, cm, tm, st);
}

void BakeryAPI::format_list_element(const std::string& b_id, const std::string& or_id, const std::string& cus_name, const std::string& o_time,
                                    const std::string& o_status) {
    std::string customer_name = cus_name;
    if (customer_name.size() > C_W-5 ){
        customer_name = customer_name.substr(0, C_W-5);
    }
    std::cout << std::setw(ID_W) << b_id << std::setw(OID_W) << or_id << std::setw(C_W)
              << customer_name << std::setw(T_W) << o_time << std::setw(S_W) << o_status << std::endl;
}

void BakeryAPI::print_command_header() {
    int total_w = ID_W+OID_W+C_W+T_W+S_W;
    int split = total_w/3;
    std::string q_command = "QUIT [q]";
    std::string u_command = "UPDATE [u]";
    std::string d_command = "DETAILS [d]";
    for (int i=0; i<total_w; i++)
        std::cout << "-";

    std::cout << std::endl;
    std::cout << std::setw(split) << q_command << std::setw(split) << u_command << std::setw(split) << d_command << std::endl;

    for (int i=0; i<total_w; i++)
        std::cout << "-";
    std::cout << std::endl;
}

void BakeryAPI::print_order_details(int order_id) {
    BakerOrder* baker_order = pizzaBaker.get_order(order_id);
    if (baker_order == nullptr) return;
    terminal_mutex.lock();
    std::cout << "--- ORDER " << baker_order->order->orderID << " --- " << std::endl;
    pizzaPrinter.print(baker_order->order->pizzas);

    std::cout << "Press any key to continue...";
    terminal_mutex.unlock();
    std::string input;
    getline(std::cin, input);
}


int get_number_input()
{
    std::string input;
    int number = 0;
    while(true)
    {
        getline(std::cin, input);
        std::stringstream str_stream(input);
        if (str_stream >> number) break;
        std::cout << "Not a number." << std::endl;
    }
    return number;
}


bool BakeryAPI::get_command_input() {
    std::string input;
    while(true)
    {
        terminal_mutex.lock();
        std::cout << "Enter a command: " << std::endl;
        terminal_mutex.unlock();
        getline(std::cin, input);
        if (input == "q"){
            return false;
        }
        if (input == "u"){
            update_order();
            return true;
        }
        if (input == "d"){
            order_details();
            return true;
        }
    }
}

void BakeryAPI::order_details() {
    using_window = true;
    terminal_mutex.lock();
    std::cout << "Enter ID: ";
    terminal_mutex.unlock();
    int number = get_number_input();
    if (!pizzaBaker.exists(number)){
        terminal_mutex.lock();
        std::cout << "Selected order does not exist." << std::endl;
        terminal_mutex.unlock();
    }
    else print_order_details(number);
    using_window = false;
}

void BakeryAPI::update_order() {
    using_window = true;
    terminal_mutex.lock();
    std::cout << "Enter ID: ";
    terminal_mutex.unlock();
    int number = get_number_input();
    bool exists = pizzaBaker.exists(number);
    if (!exists){
        terminal_mutex.lock();
        std::cout << "Selected order does not exist." << std::endl;
        terminal_mutex.unlock();
    }
    else {
        pizzaBaker.update_order_status(number);
        BakerOrder* baker_order = pizzaBaker.get_order(number);
        if (baker_order->getState() == COMPLETE) notify_completion(*baker_order);
        else send_status_update(*baker_order);
    }
    using_window = false;
}

void BakeryAPI::add_client(Client *client_socket) {
    client = client_socket;
}

void BakeryAPI::start() {
    bool stay_alive = true;
    while(stay_alive)
    {
        print_orders();
        stay_alive = get_command_input();
    }
}

void BakeryAPI::server_response_handler(Response *response) {
    if (response->getStatus() == SUCCESS)
    {
        switch(response->getType())
        {
            case RES_MESSAGE:{
                terminal_mutex.lock();
                std::string message_from_server = Parser::parse_message(response);
                std::cout << "Message from server: " << message_from_server << std::endl;
                terminal_mutex.unlock();
                return;
            }
            case RES_ORDER: {
                PizzaOrder* pizza_order = Parser::parse_order(response);
                pizzaBaker.add_order(pizza_order);
                if (!using_window)
                    print_orders();
                return;
            }
            default:{
                terminal_mutex.lock();
                std::cout << "Received an unrecognized response from server" << std::endl;
                terminal_mutex.unlock();
                return;
            }
        }
    }
    if (response->getStatus() == ERROR)
    {
        switch(response->getType())
        {
            case RES_MESSAGE:{
                terminal_mutex.lock();
                std::string message_from_server = Parser::parse_message(response);
                std::cout << "Error message from server: " << message_from_server << std::endl;
                terminal_mutex.unlock();
                return;
            }
            default:{
                terminal_mutex.lock();
                std::cout << "Received an un-parsable error message from server" << std::endl;
                terminal_mutex.unlock();
                return;
            }
        }
    }
    if (response->getStatus() == HANDSHAKE_SUCCESS)
    {
        std::string message_from_server = Parser::parse_message(response);
        terminal_mutex.lock();
        std::cout << "Message from server: " << message_from_server << std::endl;
        terminal_mutex.unlock();
        return;
    }
    terminal_mutex.lock();
    std::cout << "Received an unrecognized status type from server" << std::endl;
    terminal_mutex.unlock();
}

void BakeryAPI::send_status_update(BakerOrder& baker_order) {
    std::string message = "Order " + baker_order.order->orderID + " status is " + baker_order.status_string;
    json msg = {{"orderID", baker_order.order->orderID}, {"message", message}};
    auto msg_prt = new json(msg);
    Request request = Request(REQ_BAKER_STATUS, "Bakery", *msg_prt, msg_prt);
    client->sendToServer(request);
}

void BakeryAPI::notify_completion(BakerOrder& baker_order) {
    std::string message = "Order " + baker_order.order->orderID + " status is " + baker_order.status_string;
    json o_id = {{"orderID", baker_order.order->orderID}, {"message", message}};
    auto j_ptr_order = new json(o_id);
    PizzaOrder::to_json(*baker_order.order, *j_ptr_order);
    std::cout << "The dump: " << j_ptr_order->dump() << std::endl;
    Request request = Request(REQ_BAKING_COMPLETE, "Bakery", *j_ptr_order, j_ptr_order);
    client->sendToServer(request);
}













