#include "PizzaPrinter.h"
#include "../FileReader/FileReader.h"
#include <iostream>

bool sort_by_type(pizza_topping_serializer::PizzaTopping* a, pizza_topping_serializer::PizzaTopping* b)
{
    return (a->type<b->type);
}

PizzaPrinter::PizzaPrinter()
{
    FileReader fileReader;
    sizes        = fileReader.getSizes();
    bottoms      = fileReader.getBottoms();
    toppings     = fileReader.getToppings();
    toppingTypes = fileReader.getToppingTypes();
}

void PizzaPrinter::print(Pizza &pizza)
{
    std::cout << " -- " << pizza.name << " -- " << std::endl;
    printSize(pizza.size);
    printBottom(pizza.bottom);

    std::sort(pizza.toppings.begin(), pizza.toppings.end(), sort_by_type);
    int curr_type = -1;
    for(auto topping: pizza.toppings){
        if (curr_type != topping->type)
        {
            curr_type = topping->type;
            printToppingType(topping->type);
        }
        printTopping(topping->type, topping->id, topping->amount);
    }

}

void PizzaPrinter::print(const std::vector<Pizza *>& pizzas)
{
    for (auto pizza: pizzas)
    {
        print(*pizza);
        std::cout << std::endl;
    }
}

void PizzaPrinter::printSize(int size_id)
{
    std::cout << "Size  : " << get_name(sizes, size_id) << std::endl;
}

void PizzaPrinter::printTopping(int topping_type, int topping_id, int topping_amount)
{
    if (topping_type == 4)
    {
        std::cout << "- " << get_name(toppings, topping_id) << std::endl;
        return;
    }
    std::cout << "- " << topping_amount << "x " << get_name(toppings, topping_id) << std::endl;
}

void PizzaPrinter::printBottom(int bottom_id)
{
    std::cout << "Bottom: "<< get_name(bottoms, bottom_id) << std::endl;
}

void PizzaPrinter::printToppingType(int type_id)
{
    std::cout << toppingTypes[type_id].name << ":"<< std::endl;
}

template<class T>
std::string PizzaPrinter::get_name(std::map<int, T> &list, int identifier)
{
    return list[identifier].name;
}