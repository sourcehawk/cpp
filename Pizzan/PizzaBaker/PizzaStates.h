#ifndef PIZZAN_PIZZASTATES_H
#define PIZZAN_PIZZASTATES_H

enum PizzaStates {
    RECEIVED,
    PREPARATION,
    IN_OVEN,
    COMPLETE
};

#endif //PIZZAN_PIZZASTATES_H
