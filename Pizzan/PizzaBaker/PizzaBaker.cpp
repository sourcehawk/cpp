#include "PizzaBaker.h"
#include <iostream>
#include "../FileWriter/FileWriter.h"
#include "../Networking/Parser/Parser.h"
#include "../json.hpp"
using nlohmann::json;
#define COMPLETED_ORDERS_DECAY_TIME_MIN 5

long int get_time_now(){
    return std::chrono::duration_cast<std::chrono::minutes>(std::chrono::system_clock::now().time_since_epoch()).count();
}

void PizzaBaker::add_order(PizzaOrder *pizza_order) {
    std::vector<int> to_remove = {};
    long int time_now = get_time_now();
    for (auto order: orders)
    {
        if (order.second->getState() == COMPLETE)
        {
            long int time_diff = time_now - std::chrono::duration_cast<std::chrono::minutes>(order.second->time.time_since_epoch()).count();
            std::cout << "time difference: "<< time_diff << std::endl;
            if (time_diff>COMPLETED_ORDERS_DECAY_TIME_MIN){
                to_remove.push_back(order.first);
            }
        }
    }
    for (auto id: to_remove) remove_order(id);
    orders.insert({next_id, new BakerOrder(pizza_order, RECEIVED)});
    next_id++;
}

bool PizzaBaker::exists(int id) const
{
    return orders.count(id)>0;
}

void PizzaBaker::remove_order(int id) {
    if (exists(id))
    {
        BakerOrder* order = orders[id];
        delete order;
        orders.erase(id);
        return;
    }
}

BakerOrder *PizzaBaker::get_order(int id) {
    if (exists(id))
    {
        return orders[id];
    }
    return nullptr;
}

PizzaStates PizzaBaker::get_order_state(int id){
    if (exists(id))
        return orders[id]->getState();
    return RECEIVED;
}

PizzaStates get_next_state(PizzaStates curr_state)
{
    switch(curr_state)
    {
        case RECEIVED:
            return PREPARATION;
        case PREPARATION:
            return IN_OVEN;
        case IN_OVEN:
        case COMPLETE:
            return COMPLETE;
        default:
            return RECEIVED;
    }
}


bool PizzaBaker::update_order_status(int id) {
    if (exists(id))
    {
        BakerOrder* baker_order = orders[id];
        baker_order->update_status(get_next_state(baker_order->getState()));
        if (baker_order->getState() == COMPLETE)
        {
            json json_order;
            PizzaOrder::to_json(*baker_order->order, json_order);
            std::string str_order = json_order.dump();
            FileWriter fw;
            fw.save_completed_baking(str_order);
        }
        return true;
    }
    return false;
}
