#ifndef PIZZAN_FILEREADER_H
#define PIZZAN_FILEREADER_H

/* This will not work for production. Must either find a way to
 * extract absolute path or just remove the ../ from path once in production
 * (when running executable). */
#define TOPPING_TYPES_FILE "Data/topping_types.json"
#define PIZZA_MENU_FILE "Data/pizza_menu.json"
#define TOPPINGS_FILE "Data/toppings.json"
#define BOTTOMS_FILE "Data/bottoms.json"
#define SIZES_FILE "Data/sizes.json"

#include "../Serializers/Size.h"
#include "../Serializers/Bottom.h"
#include "../Serializers/Topping.h"
#include "../Serializers/ToppingType.h"
#include "../Serializers/Pizza.h"
#include <map>
#include <vector>
/*
 * Json file reader for pizza data
 */
class FileReader {
public:
    FileReader() = default;
    std::map<int, Pizza*> getMenu();
    std::map<int, Size> getSizes();
    std::map<int, Bottom> getBottoms();
    std::map<int, ToppingType> getToppingTypes();
    std::map<int, Topping> getToppings();

protected:
    template<class T>
    void getData(const std::string & filename, std::map<int, T>& emptyVector);

    static json getJson(const std::string &filename) ;
};

#endif //PIZZAN_FILEREADER_H
