#include "FileReader.h"
#include <fstream>
#include "../json.hpp"
using nlohmann::json;

std::map<int, Size> FileReader::getSizes()
{
    std::map<int, size_serializer::Size> sizes = {};
    getData(SIZES_FILE, sizes);
    return sizes;
}

std::map<int, Bottom> FileReader::getBottoms()
{
    std::map<int, Bottom> bottoms = {};
    getData(BOTTOMS_FILE, bottoms);
    return bottoms;
}

std::map<int, ToppingType> FileReader::getToppingTypes()
{
    std::map<int, ToppingType> topping_types = {};
    getData(TOPPING_TYPES_FILE, topping_types);
    return topping_types;
}

std::map<int, Topping> FileReader::getToppings()
{
    std::map<int, Topping> toppings = {};
    getData(TOPPINGS_FILE, toppings);
    return toppings;
}

template<class T>
void FileReader::getData(const std::string &filename, std::map<int, T>& data_map)
{
    json j_dataset = getJson(filename);
    for (auto& j_data: j_dataset)                       // Iterate json array
    {
        auto data = j_data.get<T>();                    // Serialize data into a struct
        data_map.insert(std::make_pair(data.id, data)); // Push struct into map
    }
}

json FileReader::getJson(const std::string &filename) {
    json j_dataset;                  // Initialize json object
    std::ifstream in;                // Initialize read object
    in.open(filename);               // Open file in read mode

    // Validate file existence
    if (!in.is_open()) throw std::runtime_error("File not found.");

    in >> j_dataset;                 // Read entire file in json
    return j_dataset;
}

std::map<int, Pizza *> FileReader::getMenu() {
    std::map<int, Pizza*> pizzas = {};
    json data = getJson(PIZZA_MENU_FILE);
    for (const auto& pizza: data)
    {
        Pizza* new_pizza = Pizza::from_json(pizza);
        pizzas.insert(std::make_pair(new_pizza->id, new_pizza));
    }
    return pizzas;
}
