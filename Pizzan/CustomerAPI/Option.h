#ifndef PIZZAN_OPTION_H
#define PIZZAN_OPTION_H

#include <string>
#include <utility>
struct Option {
    explicit Option(std::string name): name(std::move(name)){}
    std::string name;
};

#endif //PIZZAN_OPTION_H
