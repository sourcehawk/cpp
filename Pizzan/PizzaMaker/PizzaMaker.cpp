#include "PizzaMaker.h"
#include <iostream>
#include "../FileReader/FileReader.h"

PizzaMaker::PizzaMaker()
{
    FileReader fileReader;
    sizes        = fileReader.getSizes();
    bottoms      = fileReader.getBottoms();
    toppings     = fileReader.getToppings();
    toppingTypes = fileReader.getToppingTypes();
}

void PizzaMaker::new_pizza()
{
    auto* new_pizza = new Pizza;
    current_pizza   = new_pizza;
    current_pizza->size = 1;
    current_pizza->bottom = 1;
    calculate_pizza_price();
    pizzas.insert(std::make_pair((int) pizzas.size(), new_pizza));
    current_pizza->name = "Pizza-" + std::to_string(pizzas.size()) + ". Custom";
    current_pizza_idx = (int) pizzas.size()-1;
}

void PizzaMaker::new_pizza(Pizza& template_pizza)
{
    auto* new_pizza = new Pizza;
    current_pizza   = new_pizza;
    current_pizza->size     = template_pizza.size;
    current_pizza->bottom   = template_pizza.bottom;
    current_pizza->toppings = template_pizza.toppings;
    current_pizza->name     = "Pizza-" + std::to_string(pizzas.size()) + ". " + template_pizza.name;
    calculate_pizza_price();

    pizzas.insert(std::make_pair((int) pizzas.size(),new_pizza));
    current_pizza_idx = (int) pizzas.size()-1;
}

void PizzaMaker::remove_pizza(int pizza_index)
{
    if (pizzas.size()-1<pizza_index || pizza_index<0)
    {
        std::cout << "Requested selection does not exist." << std::endl;
        return;
    }
    delete pizzas[pizza_index];
    pizzas.erase(pizza_index);
    if (pizzas.empty())
        current_pizza = nullptr;

    else if (current_pizza_idx == pizza_index)
    {
        current_pizza_idx = (int) pizzas.size()-1;
        current_pizza     = pizzas[current_pizza_idx];
        return;
    }
    if (current_pizza_idx > pizza_index)
        current_pizza_idx --;

}

void PizzaMaker::select_pizza(int pizza_index) {
    if (pizza_index > pizzas.size()-1 || pizza_index<0)
    {
        std::cout << "Requested selection does not exist." << std::endl;
        return;
    }
    current_pizza     = pizzas[pizza_index];
    current_pizza_idx = pizza_index;
}

void PizzaMaker::select_size(int size_id)
{

    if (validate(sizes, size_id) && validate_current_pizza())
    {
        current_pizza->size   = size_id;
        calculate_pizza_price();
    }
}

void PizzaMaker::select_bottom(int bottom_id)
{
    if (validate(bottoms, bottom_id) && validate_current_pizza())
    {
        if (current_pizza->bottom != -1)
        {
            int price      = get_price(bottoms, current_pizza->bottom);
            current_pizza -= price;
        }

        current_pizza->bottom = bottom_id;
        int price             = get_price(bottoms, bottom_id);
        current_pizza->price += price;
    }
}

void PizzaMaker::select_topping(int type_id, int topping_id)
{
    if (validate(toppings, topping_id) && validate_current_pizza())
        create_or_increase(type_id, topping_id);
}

void PizzaMaker::create_or_increase(int type_id, int topping_id)
{
    int price_incrementer = get_topping_incrementer(current_pizza->size);
    int price             = toppingTypes[type_id].price;
    current_pizza->price += price_incrementer*price;

    int count = 0;
    for (auto topping: current_pizza->toppings)
    {
        if (topping->type == type_id && topping->id == topping_id)
        {
            current_pizza->toppings[count]->amount ++;
            return;
        }
        count++;
    }
    current_pizza->toppings.push_back(new pizza_topping_serializer::PizzaTopping(topping_id, type_id));
}

void PizzaMaker::remove_topping(int type_id, int topping_id)
{
    if(validate(toppings, topping_id) && validate_current_pizza())
        remove_or_decrease(type_id, topping_id);
}

void PizzaMaker::remove_or_decrease(int type_id, int topping_id)
{
    int price_incrementer = get_topping_incrementer(current_pizza->size);
    int price             = toppingTypes[type_id].price;
    current_pizza->price -= price_incrementer*price;

    int count = 0;
    for (auto topping: current_pizza->toppings)
    {
        if (topping->type == type_id && topping->id == topping_id)
        {
            if (topping->amount > 1)
            {
                topping->amount--;
                return;
            }
            delete current_pizza->toppings[count];
            current_pizza->toppings.erase(current_pizza->toppings.begin() + count);
            return;
        }
        count ++;
    }
}

void PizzaMaker::clear()
{
    for (auto pizza: pizzas)
    {
        delete pizza.second;
    }
    pizzas.erase(pizzas.begin(), pizzas.end());
    current_pizza = nullptr;
    current_pizza_idx = -1;
}

bool PizzaMaker::validate_current_pizza()
{
    if (current_pizza == nullptr) return false;
    return true;
}

template<class T>
bool PizzaMaker::validate(std::map<int, T>& list, int identifier)
{
    int count = list.count(identifier);
    if (count > 0) return true;
    std::cout << "Invalid selection." << std::endl;
    return false;
}

template<class T>
int PizzaMaker::get_price(std::map<int, T> &list, int identifier)
{
    return list[identifier].price;
}


int PizzaMaker::get_price()
{
    return current_pizza->price;
}

int PizzaMaker::get_total_price()
{
    int total = 0;
    for(auto pizza: pizzas)
    {
        total += pizza.second->price;
    }
    return total;
}

Pizza &PizzaMaker::get_pizza()
{
    if (current_pizza == nullptr) throw std::runtime_error("Must create pizza first.");
    return *current_pizza;
}

std::vector<Pizza *> PizzaMaker::get_pizzas()
{
    if (pizzas.empty()) throw std::runtime_error("Must create pizza first.");
    std::vector<Pizza*> pizza_vector = {};
    for (const auto& pizza: pizzas)
        pizza_vector.push_back(pizza.second);

    return pizza_vector;
}

int PizzaMaker::get_topping_incrementer(int size_id)
{
    return sizes[size_id].topping_price_incrementer;
}

void PizzaMaker::calculate_pizza_price() {
    int price = 0;
    for (const auto& topping: current_pizza->toppings)
    {
        int topping_price = toppingTypes[topping->type].price;
        int increment = get_topping_incrementer(current_pizza->size);
        price += topping_price*increment;
    }
    price += get_price(bottoms, current_pizza->bottom);
    price += get_price(sizes, current_pizza->size);
    current_pizza->price = price;
}





