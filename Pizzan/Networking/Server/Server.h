#ifndef PIZZAN_SERVER_H
#define PIZZAN_SERVER_H
#include "../TCP/TCP.h"
#include <unordered_map>
#include <thread>
#include <chrono>
#include <vector>
#include <mutex>
#include "../ClientSocket.h"
#include "../ClientManager.h"
class Server {
public:
    [[noreturn]] Server();

private:
    int time_since_last_cleanup;
    int client_ids = 0;
    ClientManager clientManager;
    // bool running, and thread pointer
    std::vector<std::pair<bool, std::thread*>> threads = {};
    void clean_up(std::mutex &thread_state_mutex);
};


#endif //PIZZAN_SERVER_H
