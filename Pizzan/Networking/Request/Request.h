#ifndef PIZZAN_REQUEST_H
#define PIZZAN_REQUEST_H
#include "RequestTypesEnum.h"
#include "RequestHeader.h"
#include "../../json.hpp"
using json = nlohmann::json;
class Request {
public:
    /*
     * The data inserted can be retrieved on the other end after parsing by getting json at key "body".
     * Before deserializing anything the request header should be read to know what type of data is received.
     */
    ~Request();
    explicit Request(RequestTypes type, std::string sender, const json &data);
    explicit Request(RequestTypes type, std::string sender, const json &data, json::pointer j_prt);
    const json &data;
    RequestHeader header;
    // Returns json as string. Contains the keys "RequestHeader" and "body" when parsed to json
    std::string dump();
    RequestTypes getType() const;
    static Request* parse(const std::string &dumped_request);

private:
    json::pointer j_prt = nullptr;
};

#endif //PIZZAN_REQUEST_H
