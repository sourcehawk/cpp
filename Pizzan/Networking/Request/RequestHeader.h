#ifndef PIZZAN_REQUESTHEADER_H
#define PIZZAN_REQUESTHEADER_H
#include "RequestTypesEnum.h"
#include "../../json.hpp"
using json = nlohmann::json;

namespace request_header_serializer {
    struct RequestHeader {
        RequestHeader() = default;
        RequestHeader(RequestTypes type, const std::string &sender):
            type(type), sender(sender) {}

        int type = -1;
        std::string sender = "";
    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(RequestHeader, type, sender)
}
using namespace request_header_serializer;

#endif //PIZZAN_REQUESTHEADER_H
