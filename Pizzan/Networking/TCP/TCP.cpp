#include "TCP.h"
using namespace std;

bool TCP::check_and_start_if_windows() {
    #ifdef ON_WINDOWS
        WSADATA wsa_data;
        if(WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0){
            cout << "failed at WSAStartup()" << endl;
            return false;
        }
    #endif
        return true;
}

void TCP::make_socket_address(sockaddr *address, int port, std::string ip_address) {
    sockaddr_in *socket_address = (sockaddr_in*)address;
    socket_address->sin_family = AF_INET;
    socket_address->sin_port = htons(port);
    if(ip_address == ""){
        socket_address->sin_addr.s_addr = INADDR_ANY;
    }
    else{
        socket_address->sin_addr.s_addr = inet_addr(ip_address.c_str());
    }
}

bool TCP::connect_to_server(SOCKET_TYPE &my_socket, sockaddr &socket_address) {
    if(connect(my_socket, (sockaddr *)&socket_address, sizeof(socket_address)) < 0){
        //cout << "connect() failed: " << endl;
        return false;
    }
    return true;
}
