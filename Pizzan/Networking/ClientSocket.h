#ifndef PIZZAN_CLIENTSOCKET_H
#define PIZZAN_CLIENTSOCKET_H
#include <string>
#include <vector>
#include "SocketTypesEnum.h"
#include "TCP/TCP.h"
#include <mutex>

class ClientSocket {
public:
    ClientSocket(int socket_id, SocketTypes socketType, SOCKET_TYPE client_socket, std::string orderID){
        id          = socket_id;
        name        = "Client-"+std::to_string(socket_id);
        socket      = client_socket;
        socket_type = socketType;
        order_id.push_back(orderID);
    }
    ClientSocket(int socket_id, SocketTypes socketType, SOCKET_TYPE client_socket){
        id          = socket_id;
        name        = "Client-"+std::to_string(socket_id);
        socket      = client_socket;
        socket_type = socketType;
    }
    int id;
    std::vector<std::string> order_id = {}; // If type is PIZZA_MAKER associate the socket with an order
    std::string name;
    SOCKET_TYPE socket;
    SocketTypes socket_type;
    std::mutex sender_mutex;
};

#endif //PIZZAN_CLIENTSOCKET_H
