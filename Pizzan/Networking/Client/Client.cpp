#include "Client.h"
#include <iostream>
#include <thread>
#include <mutex>
#include "../../json.hpp"
using nlohmann::json;
#define MAX_ADDR_NUM 99
#define BASE_CONNECT_STRING "127.0.0."
#define CLIENT_MAX_BUFFER 20000

/* This method utilizes an external function to process the server response. */
void process_server_response(const std::string &server_response, std::function<void(Response*)> res_processor)
{
    Response* response = Response::parse(server_response); // Parse json string to response
    res_processor(response);                               // Call the response handler set in constructor
}

/* Listens for incoming responses from the server and sends the responses to an external processing function */
void receive_from_server(SOCKET_TYPE &client_socket, std::mutex &print_mutex, std::function<void(Response*)> res_processor){
    std::mutex process_mutex;
    while(true)
    {
        char buffer[CLIENT_MAX_BUFFER] = {'\0'};
        int received_size = recv(client_socket, buffer, CLIENT_MAX_BUFFER, 0);
        if (received_size <= 0)
        {
            print_mutex.lock();
            std::cout << "A server error occurred. Terminating." << std::endl;
            print_mutex.unlock();
            return;
        }
        std::string server_response(buffer);
        process_mutex.lock();
        process_server_response(server_response, res_processor);
        process_mutex.unlock();
    }
}

Client::Client(SocketTypes socket_type, std::function<void(Response*)> response_processor):
    type(socket_type), response_processor(response_processor) {}

/* Send a request to the server. Response is handled by the function given in constructor */
void Client::sendToServer(Request &request) {
    std::lock_guard<std::mutex> guard(send_mutex);
    std::string request_dump = request.dump();
    send(client_socket, request_dump.c_str(), request_dump.size(), 0);
}

/* Makes the initial handshake to identify as a specific socket type with sever. */
void Client::makeHandshake() {
    int num_type = type;
    json handshake;
    handshake["handshake"] = num_type;
    Request request(REQ_HANDSHAKE, "client", handshake);
    sendToServer(request);
}

void Client::start() {
    int addr_num = 1;
    std::mutex print_mutex; // Mutex for printing certain messages on external threads
    std::string addr_num_str = "1";
    TCP tcp;
    SOCKET_TYPE c_socket = socket(AF_INET, SOCK_STREAM, 0);
    sockaddr socket_address;

    while(addr_num<MAX_ADDR_NUM)
    {
        std::string connect_string = BASE_CONNECT_STRING+addr_num_str;
        tcp.make_socket_address(&socket_address, TCP_PORT, connect_string);
        bool isConnected = tcp.connect_to_server(c_socket, socket_address);
        if (isConnected)
        {
            print_mutex.lock();
            std::cout << "Connected to server with " << connect_string << std::endl;
            print_mutex.unlock();
            client_socket = c_socket;
            connected = true;

            // Listening for responses from the server.
            listening_thread = new std::thread(receive_from_server,
                                               std::ref(client_socket),
                                               std::ref(print_mutex),
                                               response_processor);
            // Making the handshake with server (Notifying what type of socket we are)
            makeHandshake();
            return;
        }
        addr_num++;
        addr_num_str = std::to_string(addr_num);
    }
    std::cout << "No connection could be established. Is the server running?" << std::endl;
}
