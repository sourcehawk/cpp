#ifndef PIZZAN_CLIENT_H
#define PIZZAN_CLIENT_H
#include "../SocketTypesEnum.h"
#include "../TCP/TCP.h"
#include <string>
#include "../Request/Request.h"
#include "../Response/Response.h"
#include <mutex>

class Client {
public:
    explicit Client(SocketTypes socket_type, std::function<void(Response*)> response_processor);
    void sendToServer(Request &request);
    void start();
    bool connected = false;
    std::function<void(Response*)> response_processor;
    std::mutex send_mutex;
protected:
    void makeHandshake();
    SocketTypes type;

private:
    std::thread* listening_thread = nullptr;
    SOCKET_TYPE client_socket = 0;
};


#endif //PIZZAN_CLIENT_H
