#ifndef PIZZAN_RESPONSETYPESENUM_H
#define PIZZAN_RESPONSETYPESENUM_H

enum ResponseTypes {
    RES_MESSAGE = 1, // data["body"]["message"]
    RES_BAKER_STATUS, // data["body"]["message"], data["body"]["orderID"]
    RES_ORDER // data["body"]["order"]
};

#endif //PIZZAN_RESPONSETYPESENUM_H
