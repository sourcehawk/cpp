#ifndef PIZZAN_RESPONSEHEADER_H
#define PIZZAN_RESPONSEHEADER_H
#include <string>
#include "../../json.hpp"
#include "ResponseStatusesEnum.h"
#include "ResponseTypesEnum.h"
using json = nlohmann::json;

namespace response_header_serializer {
    struct ResponseHeader {
        ResponseHeader() = default;
        ResponseHeader(ResponseStatus status, ResponseTypes type,const std::string &sender):
            status(status), type(type), sender(sender){}

        int status = -1;
        int type   = -1;
        std::string sender;

    };
    NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ResponseHeader, status, type, sender)
}
using namespace response_header_serializer;
/*
void addResponseHeader(ResponseStatus status, const std::string &sender, json &data)
{
    ResponseHeader resp_header = ResponseHeader(status, sender);
    json json_resp_header;
    json_resp_header = resp_header;
    data["ResponseHeader"] = json_resp_header;
}*/


#endif //PIZZAN_RESPONSEHEADER_H
