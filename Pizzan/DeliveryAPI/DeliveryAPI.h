#ifndef PIZZAN_DELIVERYAPI_H
#define PIZZAN_DELIVERYAPI_H

#include "../Networking/Request/Request.h"
#include "../Networking/Response/Response.h"
#include "../Networking/Client/Client.h"
#include "../Networking/SocketTypesEnum.h"
#include "../PizzaDeliverer/PizzaDeliverer.h"
#include "../PizzaPrinter/PizzaPrinter.h"
#include <mutex>

class DeliveryAPI {
public:
    void server_response_handler(Response* response);
    void start();
    void add_client(Client* client_socket);
    void print_orders();
    std::mutex terminal_mutex;
protected:
    static void print_command_header();
    static void print_list_header();
    void print_order_details(int order_id);
    bool get_command_input();
    void notify_completion(DeliveryOrder& delivery_order);

    // Set to true when user is interacting with terminal to notify an update from server after user is done using window
    bool using_window = false;
    PizzaPrinter pizzaPrinter;
    PizzaDeliverer pizzaDeliverer;
    Client* client;

    static int get_number_input();
    void update_order();
    void order_details();

    static void format_list_element(const std::string &d_id, const std::string &post, const std::string &addr,
                             const std::string &cus_name, const std::string &o_time, const std::string &o_status);
};


#endif //PIZZAN_DELIVERYAPI_H
