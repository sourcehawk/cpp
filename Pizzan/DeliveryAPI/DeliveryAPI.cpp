#include "DeliveryAPI.h"
#include "../Networking/Parser/Parser.h"
#include <iomanip>
#include <cstdlib>
#include "../json.hpp"
using nlohmann::json;
#define ID_W 10
#define ADDR_W 20
#define POST_W 10
#define C_W 20
#define T_W 20
#define S_W 20

void DeliveryAPI::server_response_handler(Response *response) {
    if (response->getStatus() == SUCCESS)
    {
        switch(response->getType())
        {
            case RES_MESSAGE:{
                std::string message_from_server = Parser::parse_message(response);
                terminal_mutex.lock();
                std::cout << "Message from server: " << message_from_server << std::endl;
                terminal_mutex.unlock();
                return;
            }
            case RES_ORDER: {
                PizzaOrder* pizza_order = Parser::parse_order(response);
                pizzaDeliverer.add_order(pizza_order);
                if (!using_window)
                    print_orders();
                return;
            }
            default:{
                terminal_mutex.lock();
                std::cout << "Received an unrecognized response from server" << std::endl;
                terminal_mutex.unlock();
                return;
            }
        }
    }
    if (response->getStatus() == ERROR)
    {
        switch(response->getType())
        {
            case RES_MESSAGE:{
                terminal_mutex.lock();
                std::string message_from_server = Parser::parse_message(response);
                std::cout << "Error message from server: " << message_from_server << std::endl;
                terminal_mutex.unlock();
                return;
            }
            default:{
                terminal_mutex.lock();
                std::cout << "Received an un-parsable error message from server" << std::endl;
                terminal_mutex.unlock();
                return;
            }
        }
    }
    if (response->getStatus() == HANDSHAKE_SUCCESS)
    {
        std::string message_from_server = Parser::parse_message(response);
        terminal_mutex.lock();
        std::cout << "Message from server: " << message_from_server << std::endl;
        terminal_mutex.unlock();
        return;
    }
    terminal_mutex.lock();
    std::cout << "Received an unrecognized status type from server" << std::endl;
    terminal_mutex.unlock();
}

void DeliveryAPI::start() {
    bool stay_alive = true;
    while(stay_alive)
    {
        print_orders();
        stay_alive = get_command_input();
    }
}

void DeliveryAPI::add_client(Client *client_socket) {
    client = client_socket;
}

void DeliveryAPI::print_orders() {
    std::lock_guard<std::mutex> guard(terminal_mutex);
    std::system("clear");
    print_command_header();
    print_list_header();
    if (pizzaDeliverer.orders.empty())
    {
        std::string msg = "NO ORDERS";
        std::cout << msg << std::endl;
        return;
    }
    for (auto& order: pizzaDeliverer.orders)
    {
        DeliveryOrder& d_order = *order.second;
        if (d_order.order->delivery)
            format_list_element(std::to_string(order.first),
                                d_order.order->deliveryInfo->postal_code,
                                d_order.order->deliveryInfo->address,
                                d_order.order->deliveryInfo->name,
                                d_order.time_string,
                                d_order.status_string);
        else {
            format_list_element(std::to_string(order.first),
                                d_order.order->deliveryInfo->postal_code,
                                d_order.order->deliveryInfo->address,
                                d_order.order->pickUpInfo->name,
                                d_order.time_string,
                                d_order.status_string);
        }
    }
}

void DeliveryAPI::print_command_header() {
    int total_w = ID_W+POST_W+ADDR_W+C_W+T_W+S_W;
    int split = total_w/3;
    std::string q_command = "QUIT [q]";
    std::string u_command = "UPDATE [u]";
    std::string d_command = "DETAILS [d]";
    for (int i=0; i<total_w; i++)
        std::cout << "-";

    std::cout << std::endl;
    std::cout << std::setw(split) << q_command << std::setw(split) << u_command << std::setw(split) << d_command << std::endl;

    for (int i=0; i<total_w; i++)
        std::cout << "-";
    std::cout << std::endl;
}

void DeliveryAPI::print_list_header() {
    std::string id, addr, post, cm, tm, st;
    id = "ID";
    post = "PCODE";
    addr = "ADDRESS";
    cm = "CUSTOMER";
    tm = "ORDER TIME";
    st = "ORDER STATUS";
    format_list_element(id, post, addr, cm, tm, st);
}

void DeliveryAPI::format_list_element(const std::string &d_id,const std::string& post, const std::string &addr, const std::string &cus_name,
                                      const std::string &o_time, const std::string &o_status) {
    std::string customer_name = cus_name;
    if (customer_name.size() > C_W-5 ){
        customer_name = customer_name.substr(0, C_W-5);
    }
    std::cout << std::setw(ID_W) << d_id << std::setw(POST_W) << post << std::setw(ADDR_W) << addr << std::setw(C_W)
              << customer_name << std::setw(T_W) << o_time << std::setw(S_W) << o_status << std::endl;
}

void DeliveryAPI::print_order_details(int order_id) {
    // print with address info
    DeliveryOrder* delivery_order = pizzaDeliverer.get_order(order_id);
    if (delivery_order == nullptr) return;
    DeliveryInfo& deliveryInfo = *delivery_order->order->deliveryInfo;
    terminal_mutex.lock();
    std::cout << "--- ORDER " << delivery_order->order->orderID << " --- " << std::endl;
    std::cout << "DELIVERY INFORMATION" << std::endl << std::endl;
    std::cout << "Name    : " << deliveryInfo.name << std::endl;
    std::cout << "Address : " << deliveryInfo.address << std::endl;
    std::cout << "City    : " << deliveryInfo.city << std::endl;
    std::cout << "Postcode: " << deliveryInfo.postal_code << std::endl;
    std::cout << "Phone Nr: " << deliveryInfo.phone << std::endl << std::endl;
    pizzaPrinter.print(delivery_order->order->pizzas);

    std::cout << "Press any key to continue...";
    terminal_mutex.unlock();
    std::string input;
    getline(std::cin, input);
}

bool DeliveryAPI::get_command_input() {
    std::string input;
    while(true)
    {
        terminal_mutex.lock();
        std::cout << "Enter a command: " << std::endl;
        terminal_mutex.unlock();
        getline(std::cin, input);
        if (input == "q"){
            return false;
        }
        if (input == "u"){
            update_order();
            return true;
        }
        if (input == "d"){
            order_details();
            return true;
        }
    }
}

void DeliveryAPI::notify_completion(DeliveryOrder &delivery_order) {
    json order_id;
    order_id["orderID"] = delivery_order.order->orderID;
    auto j_ptr_order = new json(order_id);
    PizzaOrder::to_json(*delivery_order.order, *j_ptr_order);
    Request request = Request(REQ_DELIVERY_COMPLETE, "Deliverer", *j_ptr_order, j_ptr_order);
    client->sendToServer(request);
}

void DeliveryAPI::update_order() {
    using_window = true;
    terminal_mutex.lock();
    std::cout << "Enter ID: ";
    terminal_mutex.unlock();
    int number = get_number_input();
    bool exists = pizzaDeliverer.exists(number);
    if (!exists){
        terminal_mutex.lock();
        std::cout << "Selected order does not exist." << std::endl;
        terminal_mutex.unlock();
    }
    else {
        pizzaDeliverer.update_order_status(number);
        DeliveryOrder* deliverer_order = pizzaDeliverer.get_order(number);
        if (deliverer_order->getState() == DELIVERED) notify_completion(*deliverer_order);
        notify_completion(*deliverer_order);
    }
    using_window = false;
}

int DeliveryAPI::get_number_input() {
    std::string input;
    int number = 0;
    while(true)
    {
        getline(std::cin, input);
        std::stringstream str_stream(input);
        if (str_stream >> number) break;
        std::cout << "Not a number." << std::endl;
    }
    return number;
}

void DeliveryAPI::order_details() {
    using_window = true;
    terminal_mutex.lock();
    std::cout << "Enter ID: ";
    terminal_mutex.unlock();
    int number = get_number_input();
    if (!pizzaDeliverer.exists(number)){
        terminal_mutex.lock();
        std::cout << "Selected order does not exist." << std::endl;
        terminal_mutex.unlock();
    }
    else print_order_details(number);
    using_window = false;
}
