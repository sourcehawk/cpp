#include "PizzaPicker.h"
#include <iostream>
#include "../FileReader/FileReader.h"

PizzaPicker::PizzaPicker() {
    FileReader fileReader;
    pizzas = fileReader.getMenu();
}

Pizza &PizzaPicker::select_pizza(int pizza_id) {
    for (const auto& pizza: pizzas)
        if (pizza_id == pizza.first) return *pizza.second;
    throw std::runtime_error("Selection does not exist.");
}

std::vector<Pizza *> PizzaPicker::get_pizzas() {
    std::vector<Pizza *> pizza_vec = {};
    for (const auto& pizza: pizzas)
    {
        pizza_vec.push_back(pizza.second);
    }
    return pizza_vec;
}
