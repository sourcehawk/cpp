#ifndef JSONDEMO_BOOK_H
#define JSONDEMO_BOOK_H
#include <string>

struct Book {
    Book() = default;
    Book(int isbn, std::string name, std::string author):
        isbn(isbn), name(name), author(author) {}

    int isbn;
    std::string name;
    std::string author;

    template <class Archive>
    void serialize( Archive & ar )
    {
        ar( isbn, name, author );
    }
};
#endif //JSONDEMO_BOOK_H
